package com.mico.micochange.server;

import com.alibaba.fastjson.JSONObject;
import com.mico.micochange.account.AccountQueryThread;
import com.mico.micochange.market.GetAssetPairsThread;
import com.mico.micochange.market.GetExchangeRateThread;
import com.mico.micochange.market.GetOrderBookThread;
import com.mico.micochange.market.GetTicketThread;
import com.mico.micochange.mico.SendMsgThread;
import com.mico.micochange.trade.*;
import org.apache.mina.core.service.IoHandlerAdapter;
import org.apache.mina.core.session.IoSession;

import java.util.HashMap;
import java.util.Map;

public class TimeServerHandler extends IoHandlerAdapter {

    @Override
    public void exceptionCaught(IoSession session, Throwable cause)
            throws Exception {
        cause.printStackTrace();
    }

    /*
     * 这个方法是目前这个类里最主要的，
     * 当接收到消息，只要不是quit，就把服务器当前的时间返回给客户端
     * 如果是quit，则关闭客户端连接*/
    @Override
    public void messageReceived(IoSession session, Object message)
            throws Exception {
        String str = message.toString();
        if (str.trim().equalsIgnoreCase("quit")) {
            session.close();
            return;
        }
        try {
            //处理输入参数
            JSONObject json = null;
            try {
                json = (JSONObject) JSONObject.parse(str);
            } catch (Exception ex) {
                Map<String, String> retmap = new HashMap<String, String>();
                retmap.put("result", "报文格式错误");
                String result = JSONObject.toJSONString(retmap);
                session.write(result);
                session.close();
                return;
            }

            String act = json.get("act").toString();
            if ("getacc".equals(act)) {//获取余额
                AccountQueryThread accountQueryThread = new AccountQueryThread(session, str);
                accountQueryThread.start();
            } else if ("getticket".equals(act)) {//获取行情
                GetTicketThread getTicketThread = new GetTicketThread(session, str);
                getTicketThread.start();
            } else if ("getOrder".equals(act)) {//获取订单
                GetOrderThread getOrderThread = new GetOrderThread(session, str);
                getOrderThread.start();
            } else if ("limitOrder".equals(act)) {//限价下单
                PlaceLimitOrderThread placeLimitOrderThread = new PlaceLimitOrderThread(session, str);
                placeLimitOrderThread.start();
            } else if ("marketOrder".equals(act)) {//市价下单
                PlaceMarketOrderThread placeMarketOrderThread = new PlaceMarketOrderThread(session, str);
                placeMarketOrderThread.start();
            } else if ("cancelOrder".equals(act)) {//撤单
                CancelOrderThread cancelOrderThread = new CancelOrderThread(session, str);
                cancelOrderThread.start();
            } else if ("getOpenOrders".equals(act)) {//获取持有订单
                GetOpenOrdersThread getOpenOrdersThread = new GetOpenOrdersThread(session, str);
                getOpenOrdersThread.start();
            } else if ("getDepth".equals(act)) {//获取深度
                GetOrderBookThread getOrderBookThread = new GetOrderBookThread(session, str);
                getOrderBookThread.start();
            } else if ("getAssetPairs".equals(act)) {//获取交易对
                GetAssetPairsThread getAssetPairsThread = new GetAssetPairsThread(session, str);
                getAssetPairsThread.start();
            } else if ("getExchangeRate".equals(act)) {//获取人民币美元汇率
                GetExchangeRateThread getExchangeRateThread = new GetExchangeRateThread(session, str);
                getExchangeRateThread.start();
            } else if ("sendMsg".equals(act)) {//发送短信通知
                SendMsgThread sendMsgThread = new SendMsgThread(session, str);
                sendMsgThread.start();
            } else if ("test".equals(act)) {//测试
                session.write(json.get("data").toString());
            }
        } catch (Exception ex) {
            Map ret = new HashMap();
            ret.put("err", "交易失败，请联系外联平台管理员！");
            session.write(ret);
        }
    }

    @Override
    public void sessionClosed(IoSession session) throws Exception {
        // TODO Auto-generated method stub
        super.sessionClosed(session);
//        System.out.println("客户端与服务端断开连接.....");
    }

    @Override
    public void messageSent(IoSession session, Object message) throws Exception {
        super.messageSent(session, message);
        session.close();
    }

}