package com.mico.micochange.server;

/**
 * Created by chenst on 2018/5/5.
 */

import com.mico.xchange.binance.api.client.BinanceApiClientFactory;
import com.mico.xchange.binance.api.client.BinanceApiWebSocketClient;
import com.mico.micochange.common.ConfigUtils;
import org.apache.mina.core.service.IoAcceptor;
import org.apache.mina.core.session.IdleStatus;
import org.apache.mina.filter.codec.ProtocolCodecFilter;
import org.apache.mina.filter.codec.textline.TextLineCodecFactory;
import org.apache.mina.filter.executor.ExecutorFilter;
import org.apache.mina.filter.logging.LoggingFilter;
import org.apache.mina.transport.socket.nio.NioSocketAcceptor;

import java.net.InetSocketAddress;
import java.nio.charset.Charset;
import java.util.concurrent.Executors;

/**
 * @author aniyo
 * blog: http://aniyo.iteye.com
 */
public class MinaTimeServer {

    /**
     *
     */
    public MinaTimeServer() {
        // TODO Auto-generated constructor stub
    }

    /**
     * @param args
     */
    public static void main(String[] args) {
        // 服务器端的主要对象
        IoAcceptor acceptor = new NioSocketAcceptor();

        // 设置Filter链
        acceptor.getFilterChain().addLast("logger", new LoggingFilter());

        TextLineCodecFactory textLineCodeFactory = new TextLineCodecFactory(Charset.forName("UTF-8"));
        textLineCodeFactory.setDecoderMaxLineLength(20000);
        textLineCodeFactory.setEncoderMaxLineLength(20000);
        // 协议解析，采用mina现成的UTF-8字符串处理方式
        acceptor.getFilterChain().addLast("codec", new ProtocolCodecFilter(textLineCodeFactory));

        // 设置消息处理类（创建、关闭Session，可读可写等等，继承自接口IoHandler）
        acceptor.setHandler(new TimeServerHandler());
        // 设置接收缓存区大小
        acceptor.getSessionConfig().setReadBufferSize(2048);
        acceptor.getSessionConfig().setIdleTime(IdleStatus.BOTH_IDLE, 10);  //读写通道无任何操作就进入空闲状态
        acceptor.getFilterChain().addLast("ThreadPool", new ExecutorFilter(Executors.newCachedThreadPool()));


        try {
            // 服务器开始监听
            acceptor.bind(new InetSocketAddress(Integer.parseInt(ConfigUtils.getValue("startup.port"))));
        } catch (Exception e) {
            e.printStackTrace();
        }


//        BinanceApiWebSocketClient client = BinanceApiClientFactory.newInstance().newWebSocketClient();
//        client.onDepthEvent("nulsbtc", response -> System.out.println(response));
//
    }

}