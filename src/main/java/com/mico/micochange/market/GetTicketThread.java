package com.mico.micochange.market;

import com.alibaba.fastjson.JSONObject;
import com.mico.micochange.common.ExchangeCacheUtils;
import com.mico.micochange.server.MicoThread;
import com.mico.xchange.Exchange;
import com.mico.xchange.ExchangeFactory;
import com.mico.xchange.binance.BinanceExchange;
import com.mico.xchange.binance.service.BinanceMarketDataService;
import com.mico.xchange.bitstamp.BitstampExchange;
import com.mico.xchange.bitstamp.dto.marketdata.BitstampTicker;
import com.mico.xchange.bitstamp.service.BitstampMarketDataServiceRaw;
import com.mico.xchange.bittrex.BittrexExchange;
import com.mico.xchange.bittrex.dto.marketdata.BittrexMarketSummary;
import com.mico.xchange.bittrex.dto.marketdata.BittrexTicker;
import com.mico.xchange.bittrex.service.BittrexMarketDataServiceRaw;
import com.mico.xchange.bitz.BitZExchange;
import com.mico.xchange.bitz.service.BitZMarketDataService;
import com.mico.xchange.chaoex.ChaoexExchange;
import com.mico.xchange.chaoex.dto.marketdata.ChaoexTicker;
import com.mico.xchange.chaoex.service.ChaoexMarketDataServiceRaw;
import com.mico.xchange.currency.CurrencyPair;
import com.mico.xchange.dto.marketdata.Ticker;
import com.mico.xchange.exceptions.ExchangeException;
import com.mico.xchange.huobi.HuobiExchange;
import com.mico.xchange.huobi.HuobiHadaxExchange;
import com.mico.xchange.huobi.service.HuobiMarketDataService;
import org.apache.mina.core.session.IoSession;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by chenst on 2018/5/6.
 */
public class GetTicketThread extends MicoThread {
    public GetTicketThread(IoSession session, String message) {
        super(session, message);
    }

    @Override
    public Map doHuobiChange(JSONObject json) throws Exception {
        //初始化数据
        String baseSymbol = json.get("baseSymbol").toString();
        String counterSymbol = json.get("counterSymbol").toString();

        Exchange exchange = null;
        if ("1".equals(json.getString("hadax"))) {
            exchange = ExchangeCacheUtils.getExchange(HuobiHadaxExchange.class.getName(), json.get("apiKey").toString(), json.get("secretKey").toString());
        } else {
            exchange = ExchangeCacheUtils.getExchange(HuobiExchange.class.getName(), json.get("apiKey").toString(), json.get("secretKey").toString());
        }
        HuobiMarketDataService marketService = (HuobiMarketDataService) exchange.getMarketDataService();
        CurrencyPair pair = new CurrencyPair(baseSymbol, counterSymbol);
        Map<String, Object> retMap = new HashMap<String, Object>();
        Ticker ticker = marketService.getTicker(pair);
        retMap.put("result", ticker);
        return retMap;
    }

    @Override
    public Map doBinanceChange(JSONObject json) throws Exception {
        //初始化数据
        String baseSymbol = json.get("baseSymbol").toString();
        String counterSymbol = json.get("counterSymbol").toString();

        CurrencyPair pair = new CurrencyPair(baseSymbol, counterSymbol);
        Exchange exchange = ExchangeCacheUtils.getExchange(BinanceExchange.class.getName(), json.get("apiKey").toString(), json.get("secretKey").toString());
        BinanceMarketDataService marketService = (BinanceMarketDataService) exchange.getMarketDataService();
        Map<String, Object> retMap = new HashMap<String, Object>();
        Ticker ticker = marketService.getTicker(pair);
        retMap.put("result", ticker);
//      List<BinanceKline> binanceKlines = marketService.klines(pair, KlineInterval.d1);
        return retMap;
    }

    @Override
    public Map doBitZChange(JSONObject json) throws Exception {
        //初始化数据
        String baseSymbol = json.get("baseSymbol").toString();
        String counterSymbol = json.get("counterSymbol").toString();
        CurrencyPair currencyPair = new CurrencyPair(baseSymbol, counterSymbol);

        Exchange exchange = ExchangeCacheUtils.getExchange(BitZExchange.class.getName(), json.get("apiKey").toString(), json.get("secretKey").toString());
        BitZMarketDataService marketDataService = (BitZMarketDataService) exchange.getMarketDataService();
        Map<String, Object> retMap = new HashMap<String, Object>();
        Ticker ticker = marketDataService.getTicker(currencyPair);
        retMap.put("result", ticker);
        return retMap;
    }

    @Override
    public Map doChaoexChange(JSONObject json) throws Exception {
        //初始化数据
        String baseSymbol = json.get("baseSymbol").toString();
        String counterSymbol = json.get("counterSymbol").toString();

        Exchange exchange = ExchangeFactory.INSTANCE.createExchange(ChaoexExchange.class.getName(), json.get("apiKey").toString(), json.get("secretKey").toString());
        ChaoexMarketDataServiceRaw marketDataServiceRaw = (ChaoexMarketDataServiceRaw) exchange.getMarketDataService();
        ChaoexTicker chaoexTicker = marketDataServiceRaw.getTicket(baseSymbol, counterSymbol);
        Map<String, Object> retMap = new HashMap<String, Object>();
        retMap.put("result", chaoexTicker);
        return retMap;
    }

    @Override
    public Map doBitstampChange(JSONObject json) throws Exception {
        //初始化数据
        String baseSymbol = json.get("baseSymbol").toString();
        String counterSymbol = json.get("counterSymbol").toString();
        CurrencyPair currencyPair = new CurrencyPair(baseSymbol, counterSymbol);

        Exchange exchange = ExchangeFactory.INSTANCE.createExchange(BitstampExchange.class.getName(), json.get("apiKey").toString(), json.get("secretKey").toString());
        BitstampMarketDataServiceRaw marketDataService = (BitstampMarketDataServiceRaw) exchange.getMarketDataService();

        BitstampTicker bitstampTicker = marketDataService.getBitstampTicker(currencyPair);
        Map<String, Object> retMap = new HashMap<String, Object>();
        retMap.put("result", bitstampTicker);
        return retMap;
    }

    @Override
    public Map doOkexChange(JSONObject json) throws Exception {
        return null;
    }

    @Override
    public Map doBittrexExchange(JSONObject json) throws Exception {
        //初始化数据
        String baseSymbol = json.get("baseSymbol").toString();
        String counterSymbol = json.get("counterSymbol").toString();
        CurrencyPair currencyPair = new CurrencyPair(baseSymbol, counterSymbol);

        Exchange exchange = ExchangeFactory.INSTANCE.createExchange(BittrexExchange.class.getName(), null, null);
        BittrexMarketDataServiceRaw marketDataService = (BittrexMarketDataServiceRaw) exchange.getMarketDataService();

        BittrexMarketSummary ticker = marketDataService.getBittrexMarketSummary(baseSymbol + "-" + counterSymbol);
        Map<String, Object> retMap = new HashMap<String, Object>();
        retMap.put("result", ticker);
        return retMap;
    }

    @Override
    public Map doFCoinExchange(JSONObject json) throws Exception {
        throw new ExchangeException("not imp");
    }

}
