package com.mico.micochange.market;

import com.alibaba.fastjson.JSONObject;
import com.mico.micochange.common.ExchangeCacheUtils;
import com.mico.micochange.server.MicoThread;
import com.mico.xchange.Exchange;
import com.mico.xchange.ExchangeFactory;
import com.mico.xchange.binance.BinanceExchange;
import com.mico.xchange.binance.dto.marketdata.BinanceOrderbook;
import com.mico.xchange.binance.dto.meta.exchangeinfo.Filter;
import com.mico.xchange.binance.dto.meta.exchangeinfo.Symbol;
import com.mico.xchange.binance.service.BinanceMarketDataService;
import com.mico.xchange.bitstamp.BitstampExchange;
import com.mico.xchange.bitstamp.dto.marketdata.BitstampTradingPairInfo;
import com.mico.xchange.bitstamp.service.BitstampMarketDataService;
import com.mico.xchange.bitstamp.service.BitstampMarketDataServiceRaw;
import com.mico.xchange.bittrex.BittrexExchange;
import com.mico.xchange.bittrex.dto.marketdata.BittrexSymbol;
import com.mico.xchange.bittrex.service.BittrexMarketDataServiceRaw;
import com.mico.xchange.chaoex.ChaoexExchange;
import com.mico.xchange.chaoex.dto.marketdata.ChaoexAssetPair;
import com.mico.xchange.chaoex.service.ChaoexMarketDataServiceRaw;
import com.mico.xchange.currency.CurrencyPair;
import com.mico.xchange.fcoin.FCoinExchange;
import com.mico.xchange.fcoin.dto.marketdata.FCoinPair;
import com.mico.xchange.fcoin.service.FCoinTradeServiceRaw;
import com.mico.xchange.huobi.HuobiExchange;
import com.mico.xchange.huobi.dto.marketdata.HuobiAssetPair;
import com.mico.xchange.huobi.dto.marketdata.HuobiDepth;
import com.mico.xchange.huobi.service.HuobiMarketDataService;
import com.mico.xchange.okcoin.OkCoinExchange;
import com.mico.xchange.okcoin.service.OkCoinMarketDataServiceRaw;
import org.apache.mina.core.session.IoSession;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by chenst on 2018/5/6.
 */
public class GetAssetPairsThread extends MicoThread {
    public GetAssetPairsThread(IoSession session, String message) {
        super(session, message);
    }

    @Override
    public Map doHuobiChange(JSONObject json) throws Exception {
        //初始化数据
        Exchange exchange = ExchangeCacheUtils.getExchange(HuobiExchange.class.getName(), json.get("apiKey").toString(), json.get("secretKey").toString());
        HuobiMarketDataService marketService = (HuobiMarketDataService) exchange.getMarketDataService();
        Map<String, Object> retMap = new HashMap<String, Object>();
        List<List> lst = new ArrayList<List>();
        if ("1".equals(json.get("hadax"))) {
            HuobiAssetPair[] huobiHadaxAssetPairs = marketService.getHuobiHadaxAssetPairs();
            for (HuobiAssetPair huobiAssetPair : huobiHadaxAssetPairs) {
                List<String> var = new ArrayList<>();
                var.add(huobiAssetPair.getBaseCurrency());
                var.add(huobiAssetPair.getQuoteCurrency());
                var.add("2");
                var.add(huobiAssetPair.getPricePrecision());
                var.add(huobiAssetPair.getAmountPrecision());
                var.add("");
                lst.add(var);
            }
        } else {
            HuobiAssetPair[] huobiAssetPairs = marketService.getHuobiAssetPairs();
            for (HuobiAssetPair huobiAssetPair : huobiAssetPairs) {
                List<String> map = new ArrayList<>();
                map.add(huobiAssetPair.getBaseCurrency());
                map.add(huobiAssetPair.getQuoteCurrency());
                map.add("1");
                map.add(huobiAssetPair.getPricePrecision());
                map.add(huobiAssetPair.getAmountPrecision());
                map.add("");
                lst.add(map);
            }
        }

        retMap.put("result", lst);
        return retMap;
    }

    @Override
    public Map doBinanceChange(JSONObject json) throws Exception {
        Exchange exchange = ExchangeCacheUtils.getExchange(BinanceExchange.class.getName(), json.get("apiKey").toString(), json.get("secretKey").toString());
        BinanceMarketDataService marketService = (BinanceMarketDataService) exchange.getMarketDataService();
        Symbol[] symbols = marketService.getExchangeInfo().getSymbols();
        List<List> lst = new ArrayList<List>();
        for (Symbol symbol : symbols) {
            List<String> map = new ArrayList<>();
            map.add(symbol.getBaseAsset());
            map.add(symbol.getQuoteAsset());
            map.add("1");
            String quotePrecision = "0";
            String baseAssetPrecision = "0";
            String minQty = "0";
            String maxQty = "0";
            Filter[] filters = symbol.getFilters();
            for (Filter filter : filters) {
                if ("LOT_SIZE".equals(filter.getFilterType())) {
                    String stepSize = filter.getStepSize();
                    quotePrecision = getScale(stepSize);
                    minQty = filter.getMinQty();
                    maxQty = filter.getMaxQty();
                }
                if ("PRICE_FILTER".equals(filter.getFilterType())) {
                    String tickSize = filter.getTickSize();
                    baseAssetPrecision = getScale(tickSize);
                }
            }
            map.add(baseAssetPrecision);
            map.add(quotePrecision);
            map.add(minQty);
            lst.add(map);
        }
        Map<String, Object> retMap = new HashMap<String, Object>();
        retMap.put("result", lst);
        return retMap;
    }

    private String getScale(String stepSize) {
        String var = stepSize.substring(stepSize.indexOf(".") + 1);
        if (new BigDecimal(stepSize).abs().compareTo(new BigDecimal(0)) == 0) {
            return "8";
        }
        if (var.indexOf("1") >= 0) {
            return (var.indexOf("1") + 1) + "";
        } else {
            return "0";
        }
    }

    @Override
    public Map doChaoexChange(JSONObject json) throws Exception {
        Exchange exchange = ExchangeFactory.INSTANCE.createExchange(ChaoexExchange.class.getName(), json.get("apiKey").toString(), json.get("secretKey").toString());
        ChaoexMarketDataServiceRaw marketDataServiceRaw = (ChaoexMarketDataServiceRaw) exchange.getMarketDataService();
        ChaoexAssetPair[] chaoexAssetPairs = marketDataServiceRaw.getAssetPairs();
        List<List> lst = new ArrayList<List>();
        for (ChaoexAssetPair chaoexAssetPair : chaoexAssetPairs) {
            List<String> map = new ArrayList<>();
            map.add(chaoexAssetPair.getBaseCurrency());
            map.add(chaoexAssetPair.getQuoteCurrency());
            map.add("1");
            map.add("8");
            map.add("8");
            map.add(chaoexAssetPair.getMinQty());
            lst.add(map);
        }
        Map<String, Object> retMap = new HashMap<String, Object>();
        retMap.put("result", lst);
        return retMap;
    }

    @Override
    public Map doBitstampChange(JSONObject json) throws Exception {
        Exchange exchange = ExchangeFactory.INSTANCE.createExchange(BitstampExchange.class.getName(), json.get("apiKey").toString(), json.get("secretKey").toString());
        BitstampMarketDataServiceRaw marketDataService = (BitstampMarketDataServiceRaw) exchange.getMarketDataService();
        BitstampTradingPairInfo[] bitstampTradingPairInfos = marketDataService.getSymbol();
        List<List> lst = new ArrayList<List>();
        for (BitstampTradingPairInfo bitstampTradingPairInfo : bitstampTradingPairInfos) {
            List<String> map = new ArrayList<>();
            String[] strings = bitstampTradingPairInfo.getName().split("/");
            map.add(strings[0]);
            map.add(strings[1]);
            map.add("1");
            map.add(bitstampTradingPairInfo.getBase_decimals() + "");
            map.add(bitstampTradingPairInfo.getCounter_decimals() + "");
            map.add(bitstampTradingPairInfo.getMinimum_order());
            lst.add(map);
        }
        Map<String, Object> retMap = new HashMap<String, Object>();
        retMap.put("result", lst);
        return retMap;
    }


    @Override
    public Map doOkexChange(JSONObject json) throws Exception {
        throw new Exception("not imp");
    }

    @Override
    public Map doBittrexExchange(JSONObject json) throws Exception {
        Exchange exchange = ExchangeFactory.INSTANCE.createExchange(BittrexExchange.class.getName(), null, null);
        BittrexMarketDataServiceRaw marketDataService = (BittrexMarketDataServiceRaw) exchange.getMarketDataService();
        ArrayList<BittrexSymbol> list = marketDataService.getBittrexSymbols();
        List<List> lst = new ArrayList<List>();
        for (BittrexSymbol symbol : list) {
            List<String> map = new ArrayList<>();
            map.add(symbol.getMarketCurrency());
            map.add(symbol.getBaseCurrency());
            map.add("1");
            map.add("8");
            map.add("8");
            map.add(symbol.getMinTradeSize() + "");
            lst.add(map);
        }
        Map<String, Object> retMap = new HashMap<String, Object>();
        retMap.put("result", lst);
        return retMap;
    }

    @Override
    public Map doFCoinExchange(JSONObject json) throws Exception {
        Exchange exchange = ExchangeFactory.INSTANCE.createExchange(FCoinExchange.class.getName(), null, null);
        FCoinTradeServiceRaw tradeService = (FCoinTradeServiceRaw) exchange.getTradeService();
        List<FCoinPair> list = tradeService.getSymbols();
        List<List> lst = new ArrayList<List>();
        for (FCoinPair symbol : list) {
            List<String> map = new ArrayList<>();
            map.add(symbol.getQuote_currency());
            map.add(symbol.getBase_currency());
            map.add("1");
            map.add(symbol.getPrice_decimal());
            map.add(symbol.getAmount_decimal());
            map.add("");
            lst.add(map);
        }
        Map<String, Object> retMap = new HashMap<String, Object>();
        retMap.put("result", lst);
        return retMap;
    }
}
