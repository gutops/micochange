package com.mico.micochange.market;

import com.alibaba.fastjson.JSONObject;
import com.mico.micochange.common.ExchangeCacheUtils;
import com.mico.micochange.server.MicoPublicThread;
import com.mico.micochange.server.MicoThread;
import com.mico.xchange.Exchange;
import com.mico.xchange.ExchangeFactory;
import com.mico.xchange.binance.BinanceExchange;
import com.mico.xchange.binance.dto.meta.exchangeinfo.Symbol;
import com.mico.xchange.binance.service.BinanceMarketDataService;
import com.mico.xchange.bitstamp.BitstampExchange;
import com.mico.xchange.bitstamp.dto.marketdata.BitstampTradingPairInfo;
import com.mico.xchange.bitstamp.service.BitstampMarketDataServiceRaw;
import com.mico.xchange.chaoex.ChaoexExchange;
import com.mico.xchange.chaoex.dto.marketdata.ChaoexAssetPair;
import com.mico.xchange.chaoex.service.ChaoexMarketDataServiceRaw;
import com.mico.xchange.huobi.HuobiExchange;
import com.mico.xchange.huobi.dto.marketdata.HuobiAssetPair;
import com.mico.xchange.huobi.service.HuobiMarketDataService;
import com.mico.xchange.okcoin.OkCoinExchange;
import com.mico.xchange.okcoin.dto.marketdata.OkCoinExchangeRate;
import com.mico.xchange.okcoin.service.OkCoinMarketDataServiceRaw;
import org.apache.mina.core.session.IoSession;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by chenst on 2018/5/6.
 */
public class GetExchangeRateThread extends MicoPublicThread {

    public GetExchangeRateThread(IoSession session, String str) {
        super(session, str);
    }

    @Override
    public Map deal(JSONObject json) throws Exception {
        Exchange exchange = ExchangeFactory.INSTANCE.createExchange(OkCoinExchange.class.getName(), null, null);
        OkCoinMarketDataServiceRaw marketDataServiceRaw = (OkCoinMarketDataServiceRaw) exchange.getMarketDataService();
        OkCoinExchangeRate rate = marketDataServiceRaw.getExchangeRate();
        Map<String, BigDecimal> retMap = new HashMap<String, BigDecimal>();
        retMap.put("result", rate.getRate());
        return retMap;
    }
}
