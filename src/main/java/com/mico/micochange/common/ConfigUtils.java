package com.mico.micochange.common;

import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

public class ConfigUtils {

    public static Properties properties;

    //加载配置信息
    private static void loadProperties() {
        properties = new Properties();
        // 使用ClassLoader加载properties配置文件生成对应的输入流
        InputStream in = ConfigUtils.class.getClassLoader().getResourceAsStream("app.properties");
        // 使用properties对象加载输入流
        try {
            properties.load(in);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    //获取配置信息
    public static String getValue(String key) {
        if (properties == null) {
            loadProperties();
        }
        return properties.get(key).toString();
    }

    public static void main(String[] args) {
        System.out.println(ConfigUtils.getValue("redis.ip"));
    }
}
