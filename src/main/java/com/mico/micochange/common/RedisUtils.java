package com.mico.micochange.common;

import com.mico.micochange.exception.MicoExcepiton;
import com.mico.xchange.chaoex.dto.ChaoexException;

import java.math.BigDecimal;

public class RedisUtils {

    public static BigDecimal getTicketPrice(String exchange,String baseAssert,String countAssert) throws MicoExcepiton {
        BigDecimal price = new BigDecimal(0);
        String keyVar = "";
        try {
            StringBuffer key = new StringBuffer("price:");
            key.append(exchange.toLowerCase());
            key.append(":");
            key.append(countAssert.toUpperCase()).append("/").append(baseAssert.toUpperCase());
            keyVar = key.toString();
            String strPrice = RedisPool.getJedis().get(keyVar);
            price = new BigDecimal(strPrice);
        }catch (Exception ex){
            throw new MicoExcepiton("价格获取失败"+keyVar);
        }
        return price;
    }


}
