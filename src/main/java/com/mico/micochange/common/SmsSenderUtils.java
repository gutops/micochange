package com.mico.micochange.common;

import com.alibaba.fastjson.JSONObject;
import com.aliyuncs.DefaultAcsClient;
import com.aliyuncs.IAcsClient;
import com.aliyuncs.dysmsapi.model.v20170525.SendSmsRequest;
import com.aliyuncs.dysmsapi.model.v20170525.SendSmsResponse;
import com.aliyuncs.profile.DefaultProfile;
import com.aliyuncs.profile.IClientProfile;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * 发送短信工具类
 * Created by wuyy on 2018/5/4.
 */
public class SmsSenderUtils {
	
    private static Logger logger = LoggerFactory.getLogger(SmsSenderUtils.class);


    // 产品名称:云通信短信API产品,开发者无需替换
    static final String product = "Dysmsapi";
    // 产品域名,开发者无需替换
    static final String domain = "dysmsapi.aliyuncs.com";

    // TODO 此处需要替换成开发者自己的AK(在阿里云访问控制台寻找)
    static final String accessKeyId = "LTAIoNoktIbHdf3w";
    static final String accessKeySecret = "PbxOK3mrnAe7WBKm3A6WWsHOL0BYCH";

    public static SendSmsResponse sendSms(String phoneNum,String name, String pwd)
            throws Exception {

        // 可自助调整超时时间
        System.setProperty("sun.net.client.defaultConnectTimeout", "10000");
        System.setProperty("sun.net.client.defaultReadTimeout", "10000");

        // 初始化acsClient,暂不支持region化
        IClientProfile profile = DefaultProfile.getProfile("cn-hangzhou",
                accessKeyId, accessKeySecret);
        DefaultProfile.addEndpoint("cn-hangzhou", "cn-hangzhou", product,
                domain);
        IAcsClient acsClient = new DefaultAcsClient(profile);

        // 组装请求对象-具体描述见控制台-文档部分内容
        SendSmsRequest request = new SendSmsRequest();
        // 必填:待发送手机号
        request.setPhoneNumbers(phoneNum);
        // 必填:短信签名-可在短信控制台中找到
        request.setSignName("聚惠亿家");
        // 必填:短信模板-可在短信控制台中找到
        //request.setTemplateCode("SMS_70185141");
        request.setTemplateCode("SMS_134145288");
        // 可选:模板中的变量替换JSON串,如模板内容为"亲爱的${name},您的验证码为${code}"时,此处的值为
        request.setTemplateParam("{\"status\":\""+ name +"\", \"remark\":\"" + pwd
                + "\"}");

        // 选填-上行短信扩展码(无特殊需求用户请忽略此字段)
        // request.setSmsUpExtendCode("90997");

        // 可选:outId为提供给业务方扩展字段,最终在短信回执消息中将此值带回给调用者
        request.setOutId("123");

        // hint 此处可能会抛出异常，注意catch
        SendSmsResponse sendSmsResponse = acsClient.getAcsResponse(request);
        return sendSmsResponse;
    }
    
    /**
     * 发送短信验证码
     * @param phoneNum 手机号
     * @param verCode 验证码
     */
   public static SendSmsResponse sendVC(String phoneNum,String verCode) throws Exception{
	   // 发送内容
	   String msg ="{\"pwd\":\""+ verCode +"\"}";
	   return sendMsg("SMS_123671096",phoneNum,msg);
   }
   
   /**
    * 策略通知
    * @param msg
    * @return
    */
   public static SendSmsResponse sendSteInf(JSONObject msg) throws Exception{
	// 可自助调整超时时间
       System.setProperty("sun.net.client.defaultConnectTimeout", "10000");
       System.setProperty("sun.net.client.defaultReadTimeout", "10000");

       // 初始化acsClient,暂不支持region化
       IClientProfile profile = DefaultProfile.getProfile("cn-hangzhou",
               accessKeyId, accessKeySecret);
       DefaultProfile.addEndpoint("cn-hangzhou", "cn-hangzhou", product,
               domain);
       IAcsClient acsClient = new DefaultAcsClient(profile);

       // 组装请求对象-具体描述见控制台
       SendSmsRequest request = new SendSmsRequest();
       // 必填:待发送手机号
       request.setPhoneNumbers(msg.getString("phoneNum"));//phoneNum
       // 必填:短信签名
       request.setSignName("聚惠亿家");
       // 必填:短信模板
       request.setTemplateCode("SMS_136161189");
       // 可选:模板中的变量替换JSON串,如模板内容为"亲爱的${name},您的验证码为${code}"时,此处的值为
       request.setTemplateParam("{\"steName\":\""+ msg.getString("stfName") +"\", \"dealTime\":\"" + msg.getString("dealTime")
    		   					+"\", \"coinType\":\"" + msg.getString("coinType") +"\", \"dealPrice\":\"" + msg.getString("dealPrice")
    		   					 + "\"}");
       logger.info("Setinf send msg : " + msg.toJSONString());
       SendSmsResponse sendSmsResponse = acsClient.getAcsResponse(request);
       logger.info("Setinf recive msg : " + JSONObject.toJSONString(sendSmsResponse));
       return sendSmsResponse;
   }
   
   /**
    * 消息发送
    * @param tmpCode
    * @param phoneNum
    * @param message
    * @return
    */
   private static SendSmsResponse sendMsg(String tmpCode,String phoneNum,String message) throws Exception{
	// 可自助调整超时时间
       System.setProperty("sun.net.client.defaultConnectTimeout", "10000");
       System.setProperty("sun.net.client.defaultReadTimeout", "10000");

       // 初始化acsClient,暂不支持region化
       IClientProfile profile = DefaultProfile.getProfile("cn-hangzhou",
               accessKeyId, accessKeySecret);
       DefaultProfile.addEndpoint("cn-hangzhou", "cn-hangzhou", product,
               domain);
       IAcsClient acsClient = new DefaultAcsClient(profile);

       // 组装请求对象-具体描述见控制台-文档部分内容
       SendSmsRequest request = new SendSmsRequest();
       // 必填:待发送手机号
       request.setPhoneNumbers(phoneNum);
       // 必填:短信签名-可在短信控制台中找到
       request.setSignName("聚惠亿家");
       // 必填:短信模板-可在短信控制台中找到
       request.setTemplateCode(tmpCode);
       // 发送的消息内容
       request.setTemplateParam(message);
       // 选填-上行短信扩展码(无特殊需求用户请忽略此字段)
       // request.setSmsUpExtendCode("90997");

       // 可选:outId为提供给业务方扩展字段,最终在短信回执消息中将此值带回给调用者
       request.setOutId("123");

       // hint 此处可能会抛出异常，注意catch
       SendSmsResponse sendSmsResponse = acsClient.getAcsResponse(request);
       return sendSmsResponse;
   }



    public static void send(String str,List<String> phoneNums) {

        SendSmsResponse response;
        String[] text = str.split("：");
        String name ="【禅院提示:"+text[0]+"】";
        String body = text[1];
        if(phoneNums.size()>0){
            for(String pN : phoneNums){
                try {
                	logger.info("发送手机号：" + pN + ";内容："+name + body);
                    response = sendSms(pN,name,body);
                    logger.info("Message All= " + body);
                    logger.info("Message Code= "+response.getCode());
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }

    }

    public static void sendToSelf(String str) {
        String name = "【新消息】";
        SendSmsResponse response;
        try {
            response = sendSms("13911722233",name, "请注意提取消息");
            System.out.println("Message Code= "+response.getCode());
        } catch (Exception e) {
            e.printStackTrace();
        }

    }
}
