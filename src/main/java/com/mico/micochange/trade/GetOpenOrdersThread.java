package com.mico.micochange.trade;

import com.alibaba.fastjson.JSONObject;
import com.mico.micochange.common.ExchangeCacheUtils;
import com.mico.micochange.server.MicoThread;
import com.mico.xchange.Exchange;
import com.mico.xchange.ExchangeFactory;
import com.mico.xchange.binance.BinanceExchange;
import com.mico.xchange.binance.service.BinanceTradeService;
import com.mico.xchange.bitstamp.BitstampExchange;
import com.mico.xchange.bitstamp.dto.trade.BitstampOrder;
import com.mico.xchange.bitstamp.service.BitstampTradeServiceRaw;
import com.mico.xchange.currency.CurrencyPair;
import com.mico.xchange.dto.Order;
import com.mico.xchange.dto.trade.LimitOrder;
import com.mico.xchange.dto.trade.OpenOrders;
import com.mico.xchange.huobi.HuobiExchange;
import com.mico.xchange.huobi.service.HuobiTradeService;
import com.mico.xchange.service.trade.params.orders.OpenOrdersParams;
import org.apache.mina.core.session.IoSession;

import java.io.IOException;
import java.util.*;

/**
 * Created by chenst on 2018/5/6.
 */
public class GetOpenOrdersThread extends Thread {
    public GetOpenOrdersThread(IoSession session, String message) {

    }


    public Map doHuobiChange(JSONObject json) throws Exception {
        //初始化数据
        CurrencyPair currencyPair = initData(json);

        Exchange exchange = ExchangeCacheUtils.getExchange(HuobiExchange.class.getName(), json.get("apiKey").toString(), json.get("secretKey").toString());
        HuobiTradeService tradeService = (HuobiTradeService) exchange.getTradeService();
        Map<String, Object> retMap = new HashMap<String, Object>();
        OpenOrdersParams openOrdersParams = tradeService.createOpenOrdersParams();
        OpenOrders openOrders = tradeService.getOpenOrders();
        List<LimitOrder> limitOrderList = openOrders.getOpenOrders();
        List<LimitOrder> limitOrders = new ArrayList<LimitOrder>();
        for (LimitOrder limitOrder : limitOrderList) {
            if (limitOrder.getCurrencyPair().equals(currencyPair)) {
                limitOrders.add(limitOrder);
            }
        }
        retMap.put("result", limitOrders);
        return retMap;
    }


    public Map doBinanceChange(JSONObject json) throws Exception {
        //初始化数据
        CurrencyPair currencyPair = initData(json);

        Exchange exchange = ExchangeCacheUtils.getExchange(BinanceExchange.class.getName(), json.get("apiKey").toString(), json.get("secretKey").toString());
        BinanceTradeService tradeService = (BinanceTradeService) exchange.getTradeService();
        Map<String, Object> retMap = new HashMap<String, Object>();
        OpenOrders openOrders = null;
        List<LimitOrder> limitOrders = new ArrayList<LimitOrder>();
        openOrders = tradeService.getOpenOrders(currencyPair);
        limitOrders = openOrders.getOpenOrders();
        retMap.put("result", limitOrders);
        return retMap;
    }


    public Map doChaoexChange(JSONObject json) throws Exception {
        throw new Exception("not imple");
    }

    public Map doBitstampChange(JSONObject json) throws Exception {
        //初始化数据
        CurrencyPair currencyPair = initData(json);

        Exchange exchange = ExchangeFactory.INSTANCE.createExchange(BitstampExchange.class.getName(), json.get("apiKey").toString(), json.get("secretKey").toString());
        BitstampTradeServiceRaw tradeService = (BitstampTradeServiceRaw) exchange.getTradeService();
        BitstampOrder[] orders = tradeService.getBitstampOpenOrders(currencyPair);
        Map<String, Object> retMap = new HashMap<String, Object>();
        retMap.put("result", orders);
        return retMap;
    }


    public Map doOkexChange(JSONObject json) throws Exception {
        return null;
    }


    public Map doBittrexExchange(JSONObject json) throws Exception {
        return null;
    }


    public Map doFCoinExchange(JSONObject json) throws Exception {
        return null;
    }

    public CurrencyPair initData(JSONObject json) {
        String baseSymbol = json.get("baseSymbol").toString();
        String counterSymbol = json.get("counterSymbol").toString();
        CurrencyPair currencyPair = new CurrencyPair(baseSymbol, counterSymbol);
        return currencyPair;
    }


}
