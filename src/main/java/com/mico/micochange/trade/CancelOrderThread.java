package com.mico.micochange.trade;

import com.alibaba.fastjson.JSONObject;
import com.mico.micochange.common.ExchangeCacheUtils;
import com.mico.micochange.server.MicoThread;
import com.mico.xchange.Exchange;
import com.mico.xchange.ExchangeFactory;
import com.mico.xchange.binance.BinanceExchange;
import com.mico.xchange.binance.service.BinanceCancelOrderParams;
import com.mico.xchange.binance.service.BinanceTradeService;
import com.mico.xchange.bitstamp.BitstampExchange;
import com.mico.xchange.bitstamp.service.BitstampTradeServiceRaw;
import com.mico.xchange.chaoex.ChaoexExchange;
import com.mico.xchange.chaoex.ChaoexUtils;
import com.mico.xchange.chaoex.dto.ChaoexResult;
import com.mico.xchange.chaoex.dto.ChaoexStringResult;
import com.mico.xchange.chaoex.dto.trade.ChaoexLogin;
import com.mico.xchange.chaoex.service.ChaoexTradeServiceRaw;
import com.mico.xchange.currency.CurrencyPair;
import com.mico.xchange.fcoin.FCoinExchange;
import com.mico.xchange.fcoin.service.FCoinTradeServiceRaw;
import com.mico.xchange.huobi.HuobiExchange;
import com.mico.xchange.huobi.service.HuobiTradeService;
import com.mico.xchange.okcoin.OkCoinExchange;
import com.mico.xchange.okcoin.dto.trade.OkCoinTradeResult;
import com.mico.xchange.okcoin.service.OkCoinTradeServiceRaw;
import org.apache.mina.core.session.IoSession;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by chenst on 2018/5/6.
 */
public class CancelOrderThread extends MicoThread {
    public CancelOrderThread(IoSession session, String message) {
        super(session, message);
    }


    @Override
    public Map doHuobiChange(JSONObject json) throws Exception {
        //初始化数据
        String orderId = json.get("orderId").toString();

        Exchange exchange = ExchangeCacheUtils.getExchange(HuobiExchange.class.getName(), json.get("apiKey").toString(), json.get("secretKey").toString());
        HuobiTradeService tradeService = (HuobiTradeService) exchange.getTradeService();
        Map<String, String> retMap = new HashMap<String, String>();
        boolean flag = tradeService.cancelOrder(orderId);
        retMap.put("result", flag + "");
        return retMap;
    }

    @Override
    public Map doBinanceChange(JSONObject json) throws Exception {
        //初始化数据
        String orderId = json.get("orderId").toString();

        Exchange exchange = ExchangeCacheUtils.getExchange(BinanceExchange.class.getName(), json.get("apiKey").toString(), json.get("secretKey").toString());
        BinanceTradeService tradeService = (BinanceTradeService) exchange.getTradeService();
        Map<String, String> retMap = new HashMap<String, String>();


        String baseSymbol = json.get("baseSymbol").toString();
        String counterSymbol = json.get("counterSymbol").toString();
        CurrencyPair currencyPair = new CurrencyPair(baseSymbol, counterSymbol);
        BinanceCancelOrderParams params = new BinanceCancelOrderParams(currencyPair, orderId);

        boolean flag = tradeService.cancelOrder(params);
        retMap.put("result", flag + "");
        return retMap;
    }

    @Override
    public Map doChaoexChange(JSONObject json) throws Exception {
        //初始化数据
        String orderId = json.get("orderId").toString();
        String baseSymbol = json.get("counterSymbol").toString();

        Exchange exchange = ExchangeFactory.INSTANCE.createExchange(ChaoexExchange.class.getName(), json.get("apiKey").toString(), json.get("secretKey").toString());
        ChaoexLogin chaoexLogin = ChaoexUtils.getToken(json.get("apiKey").toString(), json.get("secretKey").toString(), json.get("loginPwd").toString());
        ChaoexTradeServiceRaw chaoexTradeServiceRaw = (ChaoexTradeServiceRaw) exchange.getTradeService();
        ChaoexStringResult result = chaoexTradeServiceRaw.cancelOrder(ChaoexUtils.getCurrency(baseSymbol), orderId, chaoexLogin.getUid(), chaoexLogin.getToken());

        //防止token超时
        if ("9999".equals(result.getStatus())) {
            chaoexLogin = ChaoexUtils.getNewToken(json.get("apiKey").toString(), json.get("secretKey").toString(), json.get("loginPwd").toString());
            result = chaoexTradeServiceRaw.cancelOrder(ChaoexUtils.getCurrency(baseSymbol), orderId, chaoexLogin.getUid(), chaoexLogin.getToken());
        } else if (!result.isSuccess()) {
            throw new Exception(result.getError());
        }

        Map<String, String> retMap = new HashMap<String, String>();
        retMap.put("result", "200".equals(result.getStatus()) + "");
        return retMap;
    }

    public Map doBitstampChange(JSONObject json) throws Exception {
        //初始化数据
        String orderId = json.get("orderId").toString();

        Exchange exchange = ExchangeFactory.INSTANCE.createExchange(BitstampExchange.class.getName(), json.get("apiKey").toString(), json.get("secretKey").toString());
        BitstampTradeServiceRaw tradeService = (BitstampTradeServiceRaw) exchange.getTradeService();
        boolean flag = tradeService.cancelBitstampOrder(Integer.parseInt(orderId));


        Map<String, String> retMap = new HashMap<String, String>();
        retMap.put("result", flag + "");
        return retMap;
    }

    @Override
    public Map doOkexChange(JSONObject json) throws Exception {
        //初始化数据
        String orderId = json.get("orderId").toString();
        String baseSymbol = json.get("counterSymbol").toString();
        String counterSymbol = json.get("baseSymbol").toString();
        String symbol = counterSymbol.toLowerCase() + "_" + baseSymbol.toLowerCase();

        //现货交易
        Exchange exchange = ExchangeFactory.INSTANCE.createExchange(OkCoinExchange.class.getName(), json.get("apiKey").toString(), json.get("secretKey").toString());
        OkCoinTradeServiceRaw okCoinTradeServiceRaw = (OkCoinTradeServiceRaw) exchange.getTradeService();
        OkCoinTradeResult result = okCoinTradeServiceRaw.cancelOrder(Long.parseLong(orderId), symbol);
        Map<String, String> retMap = new HashMap<String, String>();
        retMap.put("result", result.isResult() + "");
        return retMap;
    }

    @Override
    public Map doFCoinExchange(JSONObject json) throws Exception {
        //初始化数据
        String orderId = json.get("orderId").toString();
        String baseSymbol = json.get("counterSymbol").toString();
        String counterSymbol = json.get("baseSymbol").toString();
        String symbol = counterSymbol.toLowerCase() + "_" + baseSymbol.toLowerCase();

        Exchange exchange = ExchangeFactory.INSTANCE.createExchange(FCoinExchange.class.getName(), json.get("apiKey").toString(), json.get("secretKey").toString());
        FCoinTradeServiceRaw tradeService = (FCoinTradeServiceRaw) exchange.getTradeService();
        boolean flag = tradeService.cancelOrder(orderId);
        Map<String, String> retMap = new HashMap<String, String>();
        retMap.put("result", flag + "");
        return retMap;
    }
}
