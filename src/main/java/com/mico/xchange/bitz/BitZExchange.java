package com.mico.xchange.bitz;

import com.mico.xchange.BaseExchange;
import com.mico.xchange.Exchange;
import com.mico.xchange.ExchangeSpecification;
import com.mico.xchange.bitz.service.BitZAccountDataService;
import com.mico.xchange.exceptions.NotAvailableFromExchangeException;
import com.mico.xchange.exceptions.NotYetImplementedForExchangeException;
import com.mico.xchange.service.account.AccountService;
import com.mico.xchange.service.trade.TradeService;
import com.mico.xchange.utils.nonce.CurrentTimeNonceFactory;
import com.mico.xchange.bitz.service.BitZMarketDataService;
import com.mico.xchange.bitz.service.BitZTradeService;
import si.mazi.rescu.SynchronizedValueFactory;

public class BitZExchange extends BaseExchange implements Exchange {

  private final SynchronizedValueFactory<Long> nonceFactory = new CurrentTimeNonceFactory();

  @Override
  protected void initServices() {
    this.marketDataService = new BitZMarketDataService(this);
    this.tradeService = new BitZTradeService(this);
    this.accountService = new BitZAccountDataService(this);
  }

  @Override
  public SynchronizedValueFactory<Long> getNonceFactory() {
    return nonceFactory;
  }

  @Override
  public ExchangeSpecification getDefaultExchangeSpecification() {
    ExchangeSpecification exchangeSpecification =
        new ExchangeSpecification(this.getClass().getCanonicalName());
    exchangeSpecification.setSslUri("https://www.bit-z.com");
    exchangeSpecification.setHost("http://www.bit-z.com");
    exchangeSpecification.setPort(80);
    exchangeSpecification.setExchangeName("Bit-Z");
    exchangeSpecification.setExchangeDescription(
        "Bit-Z is a Bitcoin exchange registered in Hong Kong.");

    return exchangeSpecification;
  }
}
