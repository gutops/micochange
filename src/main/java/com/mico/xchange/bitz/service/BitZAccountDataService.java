package com.mico.xchange.bitz.service;

import com.mico.xchange.Exchange;
import com.mico.xchange.bitz.dto.trade.result.BitZBalanceResult;
import com.mico.xchange.service.account.AccountService;

import java.io.IOException;

public class BitZAccountDataService extends BitZAccountDataServiceRaw implements AccountService {

  public BitZAccountDataService(Exchange exchange) {
    super(exchange);
  }


  public BitZBalanceResult getBalances() throws IOException {
    BitZBalanceResult balances = super.account(getTimestamp());
    return balances;
  }

}
