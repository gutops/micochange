package com.mico.xchange.bitz.service;

import java.io.IOException;
import java.util.List;
import com.mico.xchange.Exchange;
import com.mico.xchange.currency.CurrencyPair;
import com.mico.xchange.dto.marketdata.OrderBook;
import com.mico.xchange.dto.marketdata.Ticker;
import com.mico.xchange.dto.marketdata.Trades;
import com.mico.xchange.service.marketdata.MarketDataService;
import com.mico.xchange.bitz.BitZAdapters;
import com.mico.xchange.bitz.BitZUtils;
import com.mico.xchange.bitz.dto.marketdata.BitZKline;

public class BitZMarketDataService extends BitZMarketDataServiceRaw implements MarketDataService {

  public BitZMarketDataService(Exchange exchange) {
    super(exchange);
  }

  // X-Change Generic Services

  @Override
  public Ticker getTicker(CurrencyPair currencyPair, Object... args) throws IOException {
    return BitZAdapters.adaptTicker(
        getBitZTicker(BitZUtils.toPairString(currencyPair)), currencyPair);
  }

  @Override
  public OrderBook getOrderBook(CurrencyPair currencyPair, Object... args) throws IOException {
    return BitZAdapters.adaptOrders(
        getBitZOrders(BitZUtils.toPairString(currencyPair)), currencyPair);
  }

  @Override
  public Trades getTrades(CurrencyPair currencyPair, Object... args) throws IOException {
    return BitZAdapters.adaptTrades(
        getBitZTrades(BitZUtils.toPairString(currencyPair)), currencyPair);
  }

  // Exchange Specific Services

  public List<Ticker> getTickers(Object... args) throws IOException {
    return BitZAdapters.adaptTickers(getBitZTickerAll());
  }

  public BitZKline getKline(CurrencyPair currencyPair, String timescale) throws IOException {
    return this.getBitZKline(BitZUtils.toPairString(currencyPair), timescale);
  }
}
