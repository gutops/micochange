package com.mico.xchange.bitz;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map.Entry;
import java.util.stream.Collectors;
import java.util.stream.Stream;
import com.mico.xchange.currency.CurrencyPair;
import com.mico.xchange.dto.Order.OrderType;
import com.mico.xchange.dto.marketdata.OrderBook;
import com.mico.xchange.dto.marketdata.Ticker;
import com.mico.xchange.dto.marketdata.Trade;
import com.mico.xchange.dto.marketdata.Trades;
import com.mico.xchange.dto.trade.LimitOrder;
import com.mico.xchange.utils.DateUtils;
import com.mico.xchange.bitz.dto.marketdata.BitZOrders;
import com.mico.xchange.bitz.dto.marketdata.BitZPublicOrder;
import com.mico.xchange.bitz.dto.marketdata.BitZPublicTrade;
import com.mico.xchange.bitz.dto.marketdata.BitZTicker;
import com.mico.xchange.bitz.dto.marketdata.BitZTrades;
import com.mico.xchange.bitz.dto.marketdata.result.BitZTickerAllResult;

public class BitZAdapters {

  public static Ticker adaptTicker(BitZTicker bitzTicker, CurrencyPair currencyPair) {

    BigDecimal last = bitzTicker.getLast();
    BigDecimal bid = bitzTicker.getSell();
    BigDecimal ask = bitzTicker.getBuy();
    BigDecimal high = bitzTicker.getHigh();
    BigDecimal low = bitzTicker.getLow();
    BigDecimal volume = bitzTicker.getVolume();
    Date timestamp = DateUtils.fromMillisUtc(bitzTicker.getTimestamp());

    Ticker ticker =
        new Ticker.Builder()
            .currencyPair(currencyPair)
            .last(last)
            .bid(bid)
            .ask(ask)
            .high(high)
            .low(low)
            .volume(volume)
            .timestamp(timestamp)
            .build();

    return ticker;
  }

  public static Trade adaptTrade(BitZPublicTrade trade, CurrencyPair pair) {
    return new Trade.Builder()
        .currencyPair(pair)
        .id(String.valueOf(trade.hashCode()))
        .price(trade.getPrice())
        .originalAmount(trade.getVolume())
        .build();
  }

  public static Trades adaptTrades(BitZTrades bitZTrades, CurrencyPair pair) {
    return new Trades(
        Stream.of(bitZTrades.getTrades())
            .map(bt -> adaptTrade(bt, pair))
            .collect(Collectors.toList()));
  }

  public static OrderBook adaptOrders(BitZOrders bitZOrders, CurrencyPair currencyPair) {

    Date timestamp = DateUtils.fromMillisUtc(bitZOrders.getTimestamp());
    List<LimitOrder> asks = new ArrayList<LimitOrder>();
    List<LimitOrder> bids = new ArrayList<LimitOrder>();

    for (BitZPublicOrder order : bitZOrders.getAsks()) {
      asks.add(
          new LimitOrder.Builder(OrderType.ASK, currencyPair)
              .averagePrice(order.getPrice())
              .originalAmount(order.getVolume())
              .build());
    }

    for (BitZPublicOrder order : bitZOrders.getBids()) {
      bids.add(
          new LimitOrder.Builder(OrderType.BID, currencyPair)
              .averagePrice(order.getPrice())
              .originalAmount(order.getVolume())
              .build());
    }

    return new OrderBook(timestamp, asks, bids);
  }

  public static List<Ticker> adaptTickers(BitZTickerAllResult bitZTickerAllResult) {

    List<Ticker> tickers = new ArrayList<Ticker>();

    for (Entry<String, BitZTicker> ticker :
        bitZTickerAllResult.getData().getAllTickers().entrySet()) {
      CurrencyPair pair = BitZUtils.toCurrencyPair(ticker.getKey());

      if (pair != null) {
        tickers.add(adaptTicker(ticker.getValue(), pair));
      }
    }

    return tickers;
  }
}
