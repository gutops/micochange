package com.mico.xchange.service.trade.params.orders;

import com.mico.xchange.currency.CurrencyPair;

public interface OrderQueryParamCurrencyPair extends OrderQueryParams {
  CurrencyPair getCurrencyPair();

  void setCurrencyPair(CurrencyPair currencyPair);
}
