package com.mico.xchange.service.trade.params;

public interface CancelOrderByIdParams extends CancelOrderParams {
  String getOrderId();
}
