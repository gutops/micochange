package com.mico.xchange.service.trade.params;

import com.mico.xchange.currency.Currency;

public interface TradeHistoryParamCurrency extends TradeHistoryParams {

  Currency getCurrency();

  void setCurrency(Currency currency);
}
