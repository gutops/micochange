package com.mico.xchange.fcoin.service;

import java.io.IOException;
import com.mico.xchange.Exchange;
import com.mico.xchange.dto.Order;
import com.mico.xchange.dto.trade.LimitOrder;
import com.mico.xchange.fcoin.dto.trade.FCoinSide;
import com.mico.xchange.service.trade.TradeService;

public class FCoinTradeService extends FCoinTradeServiceRaw implements TradeService {

  public FCoinTradeService(Exchange exchange) {
    super(exchange);
  }

  private FCoinSide getSide(Order.OrderType type) {
    switch (type) {
      case ASK:
      case EXIT_BID:
        return FCoinSide.sell;
      case BID:
      case EXIT_ASK:
        return FCoinSide.buy;
    }
    return null;
  }

  @Override
  public String placeLimitOrder(LimitOrder limitOrder) throws Exception {
    String symbol =
        limitOrder.getCurrencyPair().base.getCurrencyCode().toLowerCase()
            + limitOrder.getCurrencyPair().counter.getCurrencyCode().toLowerCase();
    FCoinSide side = getSide(limitOrder.getType());
    return placeLimitOrder(
        symbol, limitOrder.getOriginalAmount(), limitOrder.getLimitPrice(), side);
  }
}
