package com.mico.xchange.fcoin.service;

import com.mico.xchange.Exchange;
import com.mico.xchange.exceptions.ExchangeException;
import com.mico.xchange.fcoin.FCoin;
import com.mico.xchange.fcoin.dto.marketdata.FCoinPair;
import com.mico.xchange.fcoin.dto.marketdata.FCoinPairsResult;
import com.mico.xchange.service.BaseExchangeService;
import com.mico.xchange.service.BaseService;
import si.mazi.rescu.ParamsDigest;
import si.mazi.rescu.RestProxyFactory;

import java.util.List;

public class FCoinBaseService extends BaseExchangeService implements BaseService {

  protected final String apiKey;
  protected final FCoin fcoin;
  protected final ParamsDigest signatureCreator;

  /**
   * Constructor
   *
   * @param exchange
   */
  public FCoinBaseService(Exchange exchange) {

    super(exchange);
    apiKey = exchange.getExchangeSpecification().getApiKey();
    fcoin =
        RestProxyFactory.createProxy(
            FCoin.class, exchange.getExchangeSpecification().getSslUri(), getClientConfig());
    signatureCreator =
        FCoinDigest.createInstance(exchange.getExchangeSpecification().getSecretKey(), apiKey);
  }

  /**
   * 获取交易交易对
   * @return
   * @throws ExchangeException
   */
  public List<FCoinPair> getSymbols() throws ExchangeException {
    FCoinPairsResult result = fcoin.getSymbols();
    if(result.isSuccess()){
      return result.getData();
    }else{
      throw new ExchangeException("Error status: " + result.getStatus() + ", " + result.getMsg());
    }
  }
}
