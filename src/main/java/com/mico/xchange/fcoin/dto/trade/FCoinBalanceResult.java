package com.mico.xchange.fcoin.dto.trade;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.List;

public class FCoinBalanceResult extends FCoinResult<List<FCoinBalance>>{

  @JsonCreator
  public FCoinBalanceResult(
      @JsonProperty("status") int status,
      @JsonProperty("data") List<FCoinBalance> data,
      @JsonProperty("msg") String msg) {
    super(status,data,msg);
  }

}
