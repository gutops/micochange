package com.mico.xchange.fcoin.dto.marketdata;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.mico.xchange.fcoin.dto.trade.FCoinOpenOrder;
import com.mico.xchange.fcoin.dto.trade.FCoinResult;

import java.util.List;

public class FCoinPairsResult extends FCoinResult<List<FCoinPair>>{

  @JsonCreator
  public FCoinPairsResult(
      @JsonProperty("status") int status,
      @JsonProperty("data") List<FCoinPair> data,
      @JsonProperty("msg") String msg) {
    super(status,data,msg);
  }

}
