package com.mico.xchange.fcoin.dto.marketdata;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.math.BigDecimal;

public final class FCoinPair {

  private final String name;
  private final String base_currency;
  private final String quote_currency;
  private final String price_decimal;
  private final String amount_decimal;


  public FCoinPair(
          @JsonProperty("name") String name,
          @JsonProperty("base_currency") String base_currency,
          @JsonProperty("quote_currency") String quote_currency,
          @JsonProperty("price_decimal") String price_decimal,
          @JsonProperty("amount_decimal") String amount_decimal){
    this.name = name;
    this.base_currency = base_currency;
    this.quote_currency = quote_currency;
    this.price_decimal = price_decimal;
    this.amount_decimal = amount_decimal;
  }

  public String getName() {
    return name;
  }

  public String getBase_currency() {
    return base_currency;
  }

  public String getQuote_currency() {
    return quote_currency;
  }

  public String getPrice_decimal() {
    return price_decimal;
  }

  public String getAmount_decimal() {
    return amount_decimal;
  }

  @Override
  public String toString() {
    return "FCoinPair{" +
            "name='" + name + '\'' +
            ", base_currency='" + base_currency + '\'' +
            ", quote_currency='" + quote_currency + '\'' +
            ", price_decimal=" + price_decimal +
            ", amount_decimal=" + amount_decimal +
            '}';
  }
}
