package com.mico.xchange.fcoin.dto.trade;

public enum FCoinSide {
  buy,
  sell
}
