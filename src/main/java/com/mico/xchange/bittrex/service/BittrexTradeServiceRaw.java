package com.mico.xchange.bittrex.service;

import java.io.IOException;
import java.util.List;
import com.mico.xchange.Exchange;
import com.mico.xchange.bittrex.BittrexUtils;
import com.mico.xchange.bittrex.dto.account.BittrexOrder;
import com.mico.xchange.bittrex.dto.account.BittrexOrderResponse;
import com.mico.xchange.bittrex.dto.trade.BittrexCancelOrderResponse;
import com.mico.xchange.bittrex.dto.trade.BittrexOpenOrder;
import com.mico.xchange.bittrex.dto.trade.BittrexOpenOrdersResponse;
import com.mico.xchange.bittrex.dto.trade.BittrexTradeHistoryResponse;
import com.mico.xchange.bittrex.dto.trade.BittrexTradeResponse;
import com.mico.xchange.bittrex.dto.trade.BittrexUserTrade;
import com.mico.xchange.currency.CurrencyPair;
import com.mico.xchange.dto.Order.OrderType;
import com.mico.xchange.dto.trade.LimitOrder;
import com.mico.xchange.dto.trade.MarketOrder;
import com.mico.xchange.exceptions.ExchangeException;
import com.mico.xchange.service.trade.params.orders.OpenOrdersParamCurrencyPair;
import com.mico.xchange.service.trade.params.orders.OpenOrdersParams;

public class BittrexTradeServiceRaw extends BittrexBaseService {

  /**
   * Constructor
   *
   * @param exchange
   */
  public BittrexTradeServiceRaw(Exchange exchange) {

    super(exchange);
  }

  public String placeBittrexMarketOrder(MarketOrder marketOrder) throws IOException {

    String pair = BittrexUtils.toPairString(marketOrder.getCurrencyPair());

    if (marketOrder.getType() == OrderType.BID) {

      BittrexTradeResponse response =
          bittrexAuthenticated.buymarket(
              apiKey,
              signatureCreator,
              exchange.getNonceFactory(),
              pair,
              marketOrder.getOriginalAmount().toPlainString());

      if (response.getSuccess()) {
        return response.getResult().getUuid();
      } else {
        throw new ExchangeException(response.getMessage());
      }

    } else {

      BittrexTradeResponse response =
          bittrexAuthenticated.sellmarket(
              apiKey,
              signatureCreator,
              exchange.getNonceFactory(),
              pair,
              marketOrder.getOriginalAmount().toPlainString());

      if (response.getSuccess()) {
        return response.getResult().getUuid();
      } else {
        throw new ExchangeException(response.getMessage());
      }
    }
  }

  public String placeBittrexLimitOrder(LimitOrder limitOrder) throws IOException {

    String pair = BittrexUtils.toPairString(limitOrder.getCurrencyPair());

    if (limitOrder.getType() == OrderType.BID) {
      BittrexTradeResponse response =
          bittrexAuthenticated.buylimit(
              apiKey,
              signatureCreator,
              exchange.getNonceFactory(),
              pair,
              limitOrder.getOriginalAmount().toPlainString(),
              limitOrder.getLimitPrice().toPlainString());

      if (response.getSuccess()) {
        return response.getResult().getUuid();
      } else {
        throw new ExchangeException(response.getMessage());
      }

    } else {
      BittrexTradeResponse response =
          bittrexAuthenticated.selllimit(
              apiKey,
              signatureCreator,
              exchange.getNonceFactory(),
              pair,
              limitOrder.getOriginalAmount().toPlainString(),
              limitOrder.getLimitPrice().toPlainString());

      if (response.getSuccess()) {
        return response.getResult().getUuid();
      } else {
        throw new ExchangeException(response.getMessage());
      }
    }
  }

  public boolean cancelBittrexLimitOrder(String uuid) throws IOException {

    BittrexCancelOrderResponse response =
        bittrexAuthenticated.cancel(apiKey, signatureCreator, exchange.getNonceFactory(), uuid);

    if (response.getSuccess()) {
      return true;
    } else {
      throw new ExchangeException(response.getMessage());
    }
  }

  public List<BittrexOpenOrder> getBittrexOpenOrders(OpenOrdersParams params) throws IOException {
    String ccyPair = null;

    if (params != null && params instanceof OpenOrdersParamCurrencyPair) {
      CurrencyPair currencyPair = ((OpenOrdersParamCurrencyPair) params).getCurrencyPair();
      if (currencyPair != null) {
        ccyPair = BittrexUtils.toPairString(currencyPair);
      }
    }

    BittrexOpenOrdersResponse response =
        bittrexAuthenticated.openorders(
            apiKey, signatureCreator, exchange.getNonceFactory(), ccyPair);

    if (response.getSuccess()) {
      return response.getBittrexOpenOrders();
    } else {
      throw new ExchangeException(response.getMessage());
    }
  }

  public List<BittrexUserTrade> getBittrexTradeHistory(CurrencyPair currencyPair)
      throws IOException {
    String ccyPair = null;
    if (currencyPair != null) ccyPair = BittrexUtils.toPairString(currencyPair);

    BittrexTradeHistoryResponse response =
        bittrexAuthenticated.getorderhistory(
            apiKey, signatureCreator, exchange.getNonceFactory(), ccyPair);

    if (response.getSuccess()) {
      return response.getResult();
    } else {
      throw new ExchangeException(response.getMessage());
    }
  }

  public BittrexOrder getBittrexOrder(String uuid) throws IOException {
    BittrexOrderResponse response =
        bittrexAuthenticated.getOrder(apiKey, signatureCreator, exchange.getNonceFactory(), uuid);

    if (response.getSuccess()) {
      return response.getResult();
    } else {
      throw new ExchangeException(response.getMessage());
    }
  }
}
