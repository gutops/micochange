package com.mico.xchange.bittrex;

import java.io.IOException;
import java.util.List;
import com.mico.xchange.BaseExchange;
import com.mico.xchange.Exchange;
import com.mico.xchange.ExchangeSpecification;
import com.mico.xchange.bittrex.dto.marketdata.BittrexSymbol;
import com.mico.xchange.bittrex.service.BittrexAccountService;
import com.mico.xchange.bittrex.service.BittrexMarketDataService;
import com.mico.xchange.bittrex.service.BittrexMarketDataServiceRaw;
import com.mico.xchange.bittrex.service.BittrexTradeService;
import com.mico.xchange.exceptions.ExchangeException;
import com.mico.xchange.utils.nonce.AtomicLongIncrementalTime2013NonceFactory;
import si.mazi.rescu.SynchronizedValueFactory;

public class BittrexExchange extends BaseExchange implements Exchange {

  private SynchronizedValueFactory<Long> nonceFactory =
      new AtomicLongIncrementalTime2013NonceFactory();

  @Override
  protected void initServices() {

    this.marketDataService = new BittrexMarketDataService(this);
    this.accountService = new BittrexAccountService(this);
    this.tradeService = new BittrexTradeService(this);
  }

  @Override
  public ExchangeSpecification getDefaultExchangeSpecification() {

    ExchangeSpecification exchangeSpecification =
        new ExchangeSpecification(this.getClass().getCanonicalName());
    exchangeSpecification.setSslUri("https://bittrex.com/api/");
    exchangeSpecification.setHost("bittrex.com");
    exchangeSpecification.setPort(80);
    exchangeSpecification.setExchangeName("Bittrex");
    exchangeSpecification.setExchangeDescription("Bittrex is a bitcoin and altcoin exchange.");

    return exchangeSpecification;
  }

  @Override
  public SynchronizedValueFactory<Long> getNonceFactory() {

    return nonceFactory;
  }

  @Override
  public void remoteInit() throws IOException, ExchangeException {

    BittrexMarketDataServiceRaw dataService = (BittrexMarketDataServiceRaw) this.marketDataService;
    List<BittrexSymbol> bittrexSymbols = dataService.getBittrexSymbols();
    exchangeMetaData = BittrexAdapters.adaptMetaData(bittrexSymbols, exchangeMetaData);
  }
}
