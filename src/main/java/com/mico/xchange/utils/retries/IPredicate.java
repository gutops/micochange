package com.mico.xchange.utils.retries;

public interface IPredicate<T> {
  boolean test(T t);
}
