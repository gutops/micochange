package com.mico.xchange.bitstamp.dto.marketdata;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.math.BigDecimal;

/** @author Matija Mazi */
public final class BitstampTradingPairInfo {

  private final String name;
  private final String url_symbol;
  private final BigDecimal base_decimals;
  private final BigDecimal counter_decimals;
  private final String minimum_order;
  private final String trading;
  private final String description;


  public BitstampTradingPairInfo(
      @JsonProperty("name") String name,
      @JsonProperty("url_symbol") String url_symbol,
      @JsonProperty("base_decimals") BigDecimal base_decimals,
      @JsonProperty("counter_decimals") BigDecimal counter_decimals,
      @JsonProperty("minimum_order") String minimum_order,
      @JsonProperty("trading") String trading,
      @JsonProperty("description") String description) {

    this.name = name;
    this.url_symbol = url_symbol;
    this.base_decimals = base_decimals;
    this.counter_decimals = counter_decimals;
    this.minimum_order = minimum_order;
    this.trading = trading;
    this.description = description;
  }

  public String getName() {
    return name;
  }

  public String getUrl_symbol() {
    return url_symbol;
  }

  public BigDecimal getBase_decimals() {
    return base_decimals;
  }

  public BigDecimal getCounter_decimals() {
    return counter_decimals;
  }

  public String getMinimum_order() {
    return minimum_order;
  }

  public String getTrading() {
    return trading;
  }

  public String getDescription() {
    return description;
  }

  @Override
  public String toString() {
    return "BitstampTradingPairInfo{" +
            "name='" + name + '\'' +
            ", url_symbol='" + url_symbol + '\'' +
            ", base_decimals=" + base_decimals +
            ", counter_decimals=" + counter_decimals +
            ", minimum_order='" + minimum_order + '\'' +
            ", trading='" + trading + '\'' +
            ", description='" + description + '\'' +
            '}';
  }
}
