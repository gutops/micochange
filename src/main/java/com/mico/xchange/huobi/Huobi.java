package com.mico.xchange.huobi;

import java.io.IOException;
import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import com.mico.xchange.huobi.dto.account.results.HuobiAccountResult;
import com.mico.xchange.huobi.dto.account.results.HuobiBalanceResult;
import com.mico.xchange.huobi.dto.marketdata.results.HuobiAssetPairsResult;
import com.mico.xchange.huobi.dto.marketdata.results.HuobiAssetsResult;
import com.mico.xchange.huobi.dto.marketdata.results.HuobiDepthResult;
import com.mico.xchange.huobi.dto.marketdata.results.HuobiTickerResult;
import com.mico.xchange.huobi.dto.trade.HuobiCreateOrderRequest;
import com.mico.xchange.huobi.dto.trade.results.HuobiCancelOrderResult;
import com.mico.xchange.huobi.dto.trade.results.HuobiOrderInfoResult;
import com.mico.xchange.huobi.dto.trade.results.HuobiOrderResult;
import com.mico.xchange.huobi.dto.trade.results.HuobiOrdersResult;
import si.mazi.rescu.ParamsDigest;

@Path("/")
@Produces(MediaType.APPLICATION_JSON)
public interface Huobi {

  @GET
  @Path("market/detail/merged")
  HuobiTickerResult getTicker(@QueryParam("symbol") String symbol) throws IOException;

  //TODO chenst
  @GET
  @Path("/market/depth")
  HuobiDepthResult getDepth(@QueryParam("symbol") String symbol, @QueryParam("type") String type) throws IOException;

  @GET
  @Path("v1/common/symbols")
  HuobiAssetPairsResult getAssetPairs() throws IOException;

  //TODO hadax
  @GET
  @Path("v1/hadax/common/symbols")
  HuobiAssetPairsResult getHadaxAssetPairs() throws IOException;

  @GET
  @Path("v1/common/currencys")
  HuobiAssetsResult getAssets() throws IOException;


  //TODO hadax
  @GET
  @Path("v1/hadax/common/currencys")
  HuobiAssetsResult getHadaxAssets() throws IOException;

  @GET
  @Path("v1/account/accounts")
  HuobiAccountResult getAccount(
          @QueryParam("AccessKeyId") String apiKey,
          @QueryParam("SignatureMethod") String signatureMethod,
          @QueryParam("SignatureVersion") int signatureVersion,
          @QueryParam("Timestamp") String nonce,
          @QueryParam("Signature") ParamsDigest signature)
      throws IOException;

  @GET
  @Path("v1/account/accounts/{account-id}/balance")
  HuobiBalanceResult getBalance(
          @PathParam("account-id") String accountID,
          @QueryParam("AccessKeyId") String apiKey,
          @QueryParam("SignatureMethod") String signatureMethod,
          @QueryParam("SignatureVersion") int signatureVersion,
          @QueryParam("Timestamp") String nonce,
          @QueryParam("Signature") ParamsDigest signature)
          throws IOException;

  //TODO hadax
  @GET
  @Path("v1/hadax/account/accounts/{account-id}/balance")
  HuobiBalanceResult getHadaxBalance(
          @PathParam("account-id") String accountID,
          @QueryParam("AccessKeyId") String apiKey,
          @QueryParam("SignatureMethod") String signatureMethod,
          @QueryParam("SignatureVersion") int signatureVersion,
          @QueryParam("Timestamp") String nonce,
          @QueryParam("Signature") ParamsDigest signature)
          throws IOException;

  @GET
  @Path("v1/order/orders")
  HuobiOrdersResult getOpenOrders(
          @QueryParam("states") String states,
          @QueryParam("AccessKeyId") String apiKey,
          @QueryParam("SignatureMethod") String signatureMethod,
          @QueryParam("SignatureVersion") int signatureVersion,
          @QueryParam("Timestamp") String nonce,
          @QueryParam("Signature") ParamsDigest signature)
      throws IOException;

  @GET
  @Path("v1/order/orders/{order-id}")
  HuobiOrderInfoResult getOrder(
          @PathParam("order-id") String orderID,
          @QueryParam("AccessKeyId") String apiKey,
          @QueryParam("SignatureMethod") String signatureMethod,
          @QueryParam("SignatureVersion") int signatureVersion,
          @QueryParam("Timestamp") String nonce,
          @QueryParam("Signature") ParamsDigest signature)
      throws IOException;

  @POST
  @Path("v1/order/orders/{order-id}/submitcancel")
  HuobiCancelOrderResult cancelOrder(
          @PathParam("order-id") String orderID,
          @QueryParam("AccessKeyId") String apiKey,
          @QueryParam("SignatureMethod") String signatureMethod,
          @QueryParam("SignatureVersion") int signatureVersion,
          @QueryParam("Timestamp") String nonce,
          @QueryParam("Signature") ParamsDigest signature)
      throws IOException;

  @POST
  @Path("v1/order/orders/place")
  @Consumes(MediaType.APPLICATION_JSON)
  HuobiOrderResult placeLimitOrder(
          HuobiCreateOrderRequest body,
          @QueryParam("AccessKeyId") String apiKey,
          @QueryParam("SignatureMethod") String signatureMethod,
          @QueryParam("SignatureVersion") int signatureVersion,
          @QueryParam("Timestamp") String nonce,
          @QueryParam("Signature") ParamsDigest signature)
          throws IOException;

  //TODO hadax
  @POST
  @Path("v1/hadax/order/orders/place")
  @Consumes(MediaType.APPLICATION_JSON)
  HuobiOrderResult placeHadaxLimitOrder(
          HuobiCreateOrderRequest body,
          @QueryParam("AccessKeyId") String apiKey,
          @QueryParam("SignatureMethod") String signatureMethod,
          @QueryParam("SignatureVersion") int signatureVersion,
          @QueryParam("Timestamp") String nonce,
          @QueryParam("Signature") ParamsDigest signature)
          throws IOException;

  @POST
  @Path("v1/order/orders/place")
  @Consumes(MediaType.APPLICATION_JSON)
  HuobiOrderResult placeMarketOrder(
          HuobiCreateOrderRequest body,
          @QueryParam("AccessKeyId") String apiKey,
          @QueryParam("SignatureMethod") String signatureMethod,
          @QueryParam("SignatureVersion") int signatureVersion,
          @QueryParam("Timestamp") String nonce,
          @QueryParam("Signature") ParamsDigest signature)
          throws IOException;

  //TODO hadax
  @POST
  @Path("v1/hadax/order/orders/place")
  @Consumes(MediaType.APPLICATION_JSON)
  HuobiOrderResult placeHadaxMarketOrder(
          HuobiCreateOrderRequest body,
          @QueryParam("AccessKeyId") String apiKey,
          @QueryParam("SignatureMethod") String signatureMethod,
          @QueryParam("SignatureVersion") int signatureVersion,
          @QueryParam("Timestamp") String nonce,
          @QueryParam("Signature") ParamsDigest signature)
          throws IOException;
}
