package com.mico.xchange.huobi.service;

import java.io.IOException;
import com.mico.xchange.Exchange;
import com.mico.xchange.currency.CurrencyPair;
import com.mico.xchange.dto.marketdata.Ticker;
import com.mico.xchange.huobi.HuobiAdapters;
import com.mico.xchange.service.marketdata.MarketDataService;

public class HuobiMarketDataService extends HuobiMarketDataServiceRaw implements MarketDataService {

  public HuobiMarketDataService(Exchange exchange) {
    super(exchange);
  }

  @Override
  public Ticker getTicker(CurrencyPair currencyPair, Object... args) throws IOException {
    return HuobiAdapters.adaptTicker(getHuobiTicker(currencyPair), currencyPair);
  }
}
