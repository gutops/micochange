package com.mico.xchange.huobi.service;

import java.io.IOException;
import com.mico.xchange.Exchange;
import com.mico.xchange.exceptions.ExchangeException;
import com.mico.xchange.huobi.Huobi;
import com.mico.xchange.huobi.dto.HuobiResult;
import com.mico.xchange.huobi.dto.marketdata.HuobiAsset;
import com.mico.xchange.huobi.dto.marketdata.results.HuobiAssetsResult;
import com.mico.xchange.service.BaseExchangeService;
import com.mico.xchange.service.BaseService;
import si.mazi.rescu.ParamsDigest;
import si.mazi.rescu.RestProxyFactory;

public class HuobiBaseService extends BaseExchangeService implements BaseService {

  protected Huobi huobi;
  protected ParamsDigest signatureCreator;

  public HuobiBaseService(Exchange exchange) {
    super(exchange);
    huobi =
        RestProxyFactory.createProxy(
            Huobi.class, exchange.getExchangeSpecification().getSslUri(), getClientConfig());
    signatureCreator =
        HuobiDigest.createInstance(exchange.getExchangeSpecification().getSecretKey());
  }

  protected <R> R checkResult(HuobiResult<R> huobiResult) {
    if (!huobiResult.isSuccess()) {
      String huobiError = huobiResult.getError();
      if (huobiError.length() == 0) {
        throw new ExchangeException("Missing error message");
      } else {
        throw new ExchangeException(huobiError);
      }
    }
    return huobiResult.getResult();
  }

  public HuobiAsset[] getHuobiAssets(String tradeType) throws IOException {
    HuobiAssetsResult assetsResult = null;
    if("hadax".equals(tradeType)){
      assetsResult = huobi.getHadaxAssets();
    }else{
      assetsResult = huobi.getAssets();
    }
    return checkResult(assetsResult);
  }
}
