package com.mico.xchange.huobi.service;

import java.io.IOException;
import com.mico.xchange.Exchange;
import com.mico.xchange.currency.CurrencyPair;
import com.mico.xchange.huobi.HuobiUtils;
import com.mico.xchange.huobi.dto.marketdata.HuobiAssetPair;
import com.mico.xchange.huobi.dto.marketdata.HuobiDepth;
import com.mico.xchange.huobi.dto.marketdata.HuobiTicker;
import com.mico.xchange.huobi.dto.marketdata.results.HuobiAssetPairsResult;
import com.mico.xchange.huobi.dto.marketdata.results.HuobiDepthResult;
import com.mico.xchange.huobi.dto.marketdata.results.HuobiTickerResult;

public class HuobiMarketDataServiceRaw extends HuobiBaseService {

  public HuobiMarketDataServiceRaw(Exchange exchange) {
    super(exchange);
  }

  public HuobiTicker getHuobiTicker(CurrencyPair currencyPair) throws IOException {
    String huobiCurrencyPair = HuobiUtils.createHuobiCurrencyPair(currencyPair);
    HuobiTickerResult tickerResult = huobi.getTicker(huobiCurrencyPair);
    return checkResult(tickerResult);
  }

  public HuobiDepth getHuobiDepth(CurrencyPair currencyPair) throws IOException {
    String huobiCurrencyPair = HuobiUtils.createHuobiCurrencyPair(currencyPair);
    HuobiDepthResult depthResult = huobi.getDepth(huobiCurrencyPair,"step5");
    return checkResult(depthResult);
  }

  public HuobiAssetPair[] getHuobiAssetPairs() throws IOException {
    HuobiAssetPairsResult assetPairsResult = huobi.getAssetPairs();
    return checkResult(assetPairsResult);
  }
  public HuobiAssetPair[] getHuobiHadaxAssetPairs() throws IOException {
    HuobiAssetPairsResult assetPairsResult = huobi.getHadaxAssetPairs();
    return checkResult(assetPairsResult);
  }
}
