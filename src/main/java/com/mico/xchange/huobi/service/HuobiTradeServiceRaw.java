package com.mico.xchange.huobi.service;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import com.mico.xchange.Exchange;
import com.mico.xchange.dto.Order.OrderType;
import com.mico.xchange.dto.trade.LimitOrder;
import com.mico.xchange.dto.trade.MarketOrder;
import com.mico.xchange.exceptions.ExchangeException;
import com.mico.xchange.huobi.HuobiUtils;
import com.mico.xchange.huobi.dto.trade.HuobiCreateOrderRequest;
import com.mico.xchange.huobi.dto.trade.HuobiOrder;
import com.mico.xchange.huobi.dto.trade.results.HuobiCancelOrderResult;
import com.mico.xchange.huobi.dto.trade.results.HuobiOrderInfoResult;
import com.mico.xchange.huobi.dto.trade.results.HuobiOrderResult;
import com.mico.xchange.huobi.dto.trade.results.HuobiOrdersResult;

class HuobiTradeServiceRaw extends HuobiBaseService {

  HuobiTradeServiceRaw(Exchange exchange) {
    super(exchange);
  }

  HuobiOrder[] getHuobiOpenOrders() throws IOException {
    String states = "pre-submitted,submitted,partial-filled,partial-canceled,filled,canceled";
    HuobiOrdersResult result =
        huobi.getOpenOrders(
            states,
            exchange.getExchangeSpecification().getApiKey(),
            HuobiDigest.HMAC_SHA_256,
            2,
            HuobiUtils.createUTCDate(exchange.getNonceFactory()),
            signatureCreator);
    return checkResult(result);
  }

  String cancelHuobiOrder(String orderId) throws IOException {
    HuobiCancelOrderResult result =
        huobi.cancelOrder(
            orderId,
            exchange.getExchangeSpecification().getApiKey(),
            HuobiDigest.HMAC_SHA_256,
            2,
            HuobiUtils.createUTCDate(exchange.getNonceFactory()),
            signatureCreator);
    return checkResult(result);
  }

  String placeHuobiLimitOrder(LimitOrder limitOrder,String tradeType) throws IOException {
    String type;
    if (limitOrder.getType() == OrderType.BID) {
      type = "buy-limit";
    } else if (limitOrder.getType() == OrderType.ASK) {
      type = "sell-limit";
    } else {
      throw new ExchangeException("Unsupported order type.");
    }

    HuobiOrderResult result = null;
    if("hadax".equals(tradeType)){
      result = huobi.placeHadaxLimitOrder(
              new HuobiCreateOrderRequest(
                      limitOrder.getId(),
                      limitOrder.getOriginalAmount().toString(),
                      limitOrder.getLimitPrice().toString(),
                      HuobiUtils.createHuobiCurrencyPair(limitOrder.getCurrencyPair()),
                      type),
              exchange.getExchangeSpecification().getApiKey(),
              HuobiDigest.HMAC_SHA_256,
              2,
              HuobiUtils.createUTCDate(exchange.getNonceFactory()),
              signatureCreator);
    } else{
      result = huobi.placeLimitOrder(
              new HuobiCreateOrderRequest(
                      limitOrder.getId(),
                      limitOrder.getOriginalAmount().toString(),
                      limitOrder.getLimitPrice().toString(),
                      HuobiUtils.createHuobiCurrencyPair(limitOrder.getCurrencyPair()),
                      type),
              exchange.getExchangeSpecification().getApiKey(),
              HuobiDigest.HMAC_SHA_256,
              2,
              HuobiUtils.createUTCDate(exchange.getNonceFactory()),
              signatureCreator);
    }

    return checkResult(result);
  }

  String placeHuobiMarketOrder(MarketOrder limitOrder,String tradeType) throws IOException {
    String type;
    if (limitOrder.getType() == OrderType.BID) {
      type = "buy-market";
    } else if (limitOrder.getType() == OrderType.ASK) {
      type = "sell-market";
    } else {
      throw new ExchangeException("Unsupported order type.");
    }
    HuobiOrderResult result = null;
    if("hadax".equals(tradeType)){
      result = huobi.placeHadaxMarketOrder(
              new HuobiCreateOrderRequest(
                      limitOrder.getId(),
                      limitOrder.getOriginalAmount().toString(),
                      null,
                      HuobiUtils.createHuobiCurrencyPair(limitOrder.getCurrencyPair()),
                      type),
              exchange.getExchangeSpecification().getApiKey(),
              HuobiDigest.HMAC_SHA_256,
              2,
              HuobiUtils.createUTCDate(exchange.getNonceFactory()),
              signatureCreator);
    }else{
      result = huobi.placeMarketOrder(
              new HuobiCreateOrderRequest(
                      limitOrder.getId(),
                      limitOrder.getOriginalAmount().toString(),
                      null,
                      HuobiUtils.createHuobiCurrencyPair(limitOrder.getCurrencyPair()),
                      type),
              exchange.getExchangeSpecification().getApiKey(),
              HuobiDigest.HMAC_SHA_256,
              2,
              HuobiUtils.createUTCDate(exchange.getNonceFactory()),
              signatureCreator);
    }

    return checkResult(result);
  }

  List<HuobiOrder> getHuobiOrder(String... orderIds) throws IOException {
    List<HuobiOrder> orders = new ArrayList<>();
    for (String orderId : orderIds) {
      HuobiOrderInfoResult orderInfoResult =
          huobi.getOrder(
              orderId,
              exchange.getExchangeSpecification().getApiKey(),
              HuobiDigest.HMAC_SHA_256,
              2,
              HuobiUtils.createUTCDate(exchange.getNonceFactory()),
              signatureCreator);
      orders.add(checkResult(orderInfoResult));
    }
    return orders;
  }
}
