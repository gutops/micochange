package com.mico.xchange.huobi.service;

import java.io.IOException;
import java.math.BigDecimal;
import java.util.List;
import com.mico.xchange.Exchange;
import com.mico.xchange.currency.Currency;
import com.mico.xchange.dto.account.AccountInfo;
import com.mico.xchange.dto.account.FundingRecord;
import com.mico.xchange.exceptions.ExchangeException;
import com.mico.xchange.huobi.HuobiAdapters;
import com.mico.xchange.huobi.dto.account.HuobiAccount;
import com.mico.xchange.service.account.AccountService;
import com.mico.xchange.service.trade.params.TradeHistoryParams;
import com.mico.xchange.service.trade.params.WithdrawFundsParams;

public class HuobiAccountService extends HuobiAccountServiceRaw implements AccountService {

  public HuobiAccountService(Exchange exchange) {
    super(exchange);
  }

  @Override
  public String withdrawFunds(WithdrawFundsParams withdrawFundsParams) throws IOException {
    return null;
  }

  @Override
  public String withdrawFunds(Currency currency, BigDecimal bigDecimal, String s)
      throws IOException {
    return null;
  }

  @Override
  public AccountInfo getAccountInfo() throws IOException {
    HuobiAccount[] accounts = getAccounts();
    if (accounts.length == 0) {
      throw new ExchangeException("Account is not recognized.");
    }
    String accountID = String.valueOf(accounts[0].getId());
    return new AccountInfo(
            accountID,
            HuobiAdapters.adaptWallet(
                    HuobiAdapters.adaptBalance(getHuobiBalance(accountID,"").getList())));
  }

  public AccountInfo getHadaxAccountInfo() throws IOException {
    HuobiAccount[] accounts = getAccounts();
    if (accounts.length == 0) {
      throw new ExchangeException("Account is not recognized.");
    }
    String accountID = String.valueOf(accounts[0].getId());
    return new AccountInfo(
            accountID,
            HuobiAdapters.adaptWallet(
                    HuobiAdapters.adaptBalance(getHuobiBalance(accountID,"hadax").getList())));
  }

  @Override
  public TradeHistoryParams createFundingHistoryParams() {
    return null;
  }

  @Override
  public List<FundingRecord> getFundingHistory(TradeHistoryParams tradeHistoryParams)
      throws IOException {
    return null;
  }

  @Override
  public String requestDepositAddress(Currency currency, String... strings) throws IOException {
    return null;
  }
}
