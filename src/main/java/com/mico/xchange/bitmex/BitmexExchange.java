package com.mico.xchange.bitmex;

import com.google.common.collect.BiMap;
import com.mico.xchange.BaseExchange;
import com.mico.xchange.Exchange;
import com.mico.xchange.ExchangeSpecification;
import com.mico.xchange.bitmex.dto.account.BitmexTicker;
import com.mico.xchange.bitmex.service.BitmexAccountService;
import com.mico.xchange.bitmex.service.BitmexMarketDataService;
import com.mico.xchange.bitmex.service.BitmexMarketDataServiceRaw;
import com.mico.xchange.bitmex.service.BitmexTradeService;
import com.mico.xchange.utils.nonce.AtomicLongIncrementalTime2013NonceFactory;
import si.mazi.rescu.SynchronizedValueFactory;

import java.io.IOException;
import java.util.List;

public class BitmexExchange extends BaseExchange implements Exchange {

  private SynchronizedValueFactory<Long> nonceFactory =
      new AtomicLongIncrementalTime2013NonceFactory();

  /** Adjust host parameters depending on exchange specific parameters */
  private static void concludeHostParams(ExchangeSpecification exchangeSpecification) {

    if (exchangeSpecification.getExchangeSpecificParameters() != null) {
      if (exchangeSpecification.getExchangeSpecificParametersItem("Use_Sandbox").equals(true)) {
        exchangeSpecification.setSslUri("https://testnet.bitmex.com/");
        exchangeSpecification.setHost("testnet.bitmex.com");
      }
    }
  }

  @Override
  public void applySpecification(ExchangeSpecification exchangeSpecification) {

    super.applySpecification(exchangeSpecification);

    concludeHostParams(exchangeSpecification);
  }

  @Override
  protected void initServices() {

    concludeHostParams(exchangeSpecification);

    this.marketDataService = new BitmexMarketDataService(this);
    this.accountService = new BitmexAccountService(this);
    this.tradeService = new BitmexTradeService(this);
  }

  @Override
  public ExchangeSpecification getDefaultExchangeSpecification() {

    ExchangeSpecification exchangeSpecification =
        new ExchangeSpecification(this.getClass().getCanonicalName());
    exchangeSpecification.setSslUri("https://www.bitmex.com/");
    exchangeSpecification.setHost("bitmex.com");
    exchangeSpecification.setPort(80);
    exchangeSpecification.setExchangeName("Bitmex");
    exchangeSpecification.setExchangeDescription("Bitmex is a bitcoin exchange");
    exchangeSpecification.setExchangeSpecificParametersItem("Use_Sandbox", false);
    return exchangeSpecification;
  }

  @Override
  public SynchronizedValueFactory<Long> getNonceFactory() {

    return nonceFactory;
  }

  @Override
  public void remoteInit() throws IOException {

    List<BitmexTicker> tickers =
        ((BitmexMarketDataServiceRaw) marketDataService).getActiveTickers();
    BiMap<BitmexPrompt, String> contracts =
        ((BitmexMarketDataServiceRaw) marketDataService).getActivePrompts(tickers);
    exchangeMetaData = BitmexAdapters.adaptToExchangeMetaData(exchangeMetaData, tickers, contracts);
  }
}
