package com.mico.xchange.bitmex.service;

import com.mico.xchange.Exchange;
import com.mico.xchange.bitmex.Bitmex;
import com.mico.xchange.bitmex.BitmexAuthenticated;
import com.mico.xchange.bitmex.BitmexException;
import com.mico.xchange.exceptions.ExchangeException;
import com.mico.xchange.exceptions.FundsExceededException;
import com.mico.xchange.exceptions.InternalServerException;
import com.mico.xchange.exceptions.RateLimitExceededException;
import com.mico.xchange.service.BaseExchangeService;
import com.mico.xchange.service.BaseService;
import si.mazi.rescu.ParamsDigest;
import si.mazi.rescu.RestProxyFactory;

public class BitmexBaseService extends BaseExchangeService implements BaseService {

  protected final Bitmex bitmex;
  protected final ParamsDigest signatureCreator;

  /**
   * Constructor
   *
   * @param exchange
   */
  public BitmexBaseService(Exchange exchange) {

    super(exchange);
    bitmex =
        RestProxyFactory.createProxy(
            BitmexAuthenticated.class,
            exchange.getExchangeSpecification().getSslUri(),
            getClientConfig());
    signatureCreator =
        BitmexDigest.createInstance(exchange.getExchangeSpecification().getSecretKey());
  }

  protected ExchangeException handleError(BitmexException exception) {

    if (exception.getMessage().contains("Insufficient")) {
      return new FundsExceededException(exception);
    } else if (exception.getMessage().contains("Rate limit exceeded")) {
      return new RateLimitExceededException(exception);
    } else if (exception.getMessage().contains("Internal server error")) {
      return new InternalServerException(exception);
    } else {
      return new ExchangeException(exception);
    }
  }
}
