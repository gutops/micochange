package com.mico.xchange.okcoin.service;

import com.mico.xchange.Exchange;
import com.mico.xchange.currency.Currency;
import com.mico.xchange.dto.account.AccountInfo;
import com.mico.xchange.exceptions.NotAvailableFromExchangeException;
import com.mico.xchange.okcoin.OkCoinAdapters;
import com.mico.xchange.service.account.AccountService;
import com.mico.xchange.service.trade.params.TradeHistoryParams;

import java.io.IOException;
import java.math.BigDecimal;

public class OkCoinFuturesAccountService extends OkCoinAccountServiceRaw implements AccountService {

  /**
   * Constructor
   *
   * @param exchange
   */
  public OkCoinFuturesAccountService(Exchange exchange) {

    super(exchange);
  }

  @Override
  public AccountInfo getAccountInfo() throws IOException {

    return OkCoinAdapters.adaptAccountInfoFutures(getFutureUserInfo());
  }

  @Override
  public String withdrawFunds(Currency currency, BigDecimal amount, String address)
      throws IOException {

    throw new NotAvailableFromExchangeException();
  }

  @Override
  public String requestDepositAddress(Currency currency, String... args) throws IOException {

    throw new NotAvailableFromExchangeException();
  }

  @Override
  public TradeHistoryParams createFundingHistoryParams() {
    throw new NotAvailableFromExchangeException();
  }
}
