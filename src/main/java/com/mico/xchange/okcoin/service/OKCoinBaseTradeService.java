package com.mico.xchange.okcoin.service;

import com.mico.xchange.Exchange;
import com.mico.xchange.exceptions.ExchangeException;
import com.mico.xchange.okcoin.OkCoin;
import com.mico.xchange.okcoin.OkCoinDigest;
import com.mico.xchange.okcoin.OkCoinUtils;
import com.mico.xchange.okcoin.dto.trade.OkCoinErrorResult;
import si.mazi.rescu.RestProxyFactory;

public class OKCoinBaseTradeService extends OkCoinBaseService {

  protected final OkCoin okCoin;
  protected final String apikey;
  protected final String secretKey;

  /**
   * Constructor
   *
   * @param exchange
   */
  protected OKCoinBaseTradeService(Exchange exchange) {

    super(exchange);

    okCoin =
        RestProxyFactory.createProxy(
            OkCoin.class, exchange.getExchangeSpecification().getSslUri(), getClientConfig());
    apikey = exchange.getExchangeSpecification().getApiKey();
    secretKey = exchange.getExchangeSpecification().getSecretKey();
  }

  protected OkCoinDigest signatureCreator() {
    return new OkCoinDigest(apikey, secretKey);
  }

  protected static <T extends OkCoinErrorResult> T returnOrThrow(T t) {

    if (t.isResult()) {
      return t;
    } else {
      throw new ExchangeException(OkCoinUtils.getErrorMessage(t.getErrorCode()));
    }
  }
}
