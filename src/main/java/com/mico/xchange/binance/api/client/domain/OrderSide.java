package com.mico.xchange.binance.api.client.domain;

/**
 * Buy/Sell order side.
 */
public enum OrderSide {
  BUY,
  SELL
}
