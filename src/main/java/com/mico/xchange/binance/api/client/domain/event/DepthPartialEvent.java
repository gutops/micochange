package com.mico.xchange.binance.api.client.domain.event;

import com.mico.xchange.binance.api.client.domain.market.OrderBookEntry;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.List;

/**
 * Depth delta event for a symbol.
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class DepthPartialEvent {


  @JsonProperty("lastUpdateId")
  private long lastUpdateId;

  /**
   * Bid depth delta.
   */
  @JsonProperty("bids")
  private List<OrderBookEntry> bids;

  /**
   * Ask depth delta.
   */
  @JsonProperty("asks")
  private List<OrderBookEntry> asks;



  public List<OrderBookEntry> getBids() {
    return bids;
  }

  public void setBids(List<OrderBookEntry> bids) {
    this.bids = bids;
  }

  public List<OrderBookEntry> getAsks() {
    return asks;
  }

  public void setAsks(List<OrderBookEntry> asks) {
    this.asks = asks;
  }

  @Override
  public String toString() {
    return "DepthPartialEvent{" +
            "lastUpdateId=" + lastUpdateId +
            ", bids=" + bids +
            ", asks=" + asks +
            '}';
  }
}
