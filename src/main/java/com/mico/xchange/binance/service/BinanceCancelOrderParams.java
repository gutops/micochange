package com.mico.xchange.binance.service;

import com.mico.xchange.currency.CurrencyPair;
import com.mico.xchange.service.trade.params.CancelOrderByCurrencyPair;
import com.mico.xchange.service.trade.params.CancelOrderByIdParams;

public class BinanceCancelOrderParams implements CancelOrderByIdParams, CancelOrderByCurrencyPair {
  private final String orderId;
  private final CurrencyPair pair;

  public BinanceCancelOrderParams(CurrencyPair pair, String orderId) {
    this.pair = pair;
    this.orderId = orderId;
  }

  @Override
  public CurrencyPair getCurrencyPair() {
    return pair;
  }

  @Override
  public String getOrderId() {
    return orderId;
  }
}
