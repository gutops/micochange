package com.mico.xchange.chaoex;

import com.mico.xchange.chaoex.dto.ChaoexException;
import com.mico.xchange.chaoex.dto.marketdata.*;
import com.mico.xchange.chaoex.dto.marketdata.results.ChaoexAssetPairsResult;
import com.mico.xchange.chaoex.dto.marketdata.results.ChaoexDepthResult;
import com.mico.xchange.chaoex.dto.marketdata.results.ChoaexTickerResult;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import java.io.IOException;
import java.util.List;

@Path("/12lian/")
@Produces(MediaType.APPLICATION_JSON)
public interface Chaoex {

  @GET
  @Path("coin/allCurrencyRelations")
  ChaoexAssetPairsResult getAssetPairs() throws IOException;

  @GET
  @Path("quote/realTime")
  ChoaexTickerResult getTicket(@QueryParam("baseCurrencyId") String baseCurrencyId,@QueryParam("tradeCurrencyId") String tradeCurrencyId) throws IOException;

  @GET
  @Path("quote/tradeDeepin")
  ChaoexDepthResult getDepth(@QueryParam("baseCurrencyId") String baseCurrencyId, @QueryParam("tradeCurrencyId") String tradeCurrencyId, @QueryParam("limit") int limit) throws IOException;

}
