package com.mico.xchange.chaoex;

import com.mico.micochange.common.ConfigUtils;
import com.mico.xchange.BaseExchange;
import com.mico.xchange.ExchangeSpecification;
import com.mico.xchange.chaoex.service.ChaoexAccountService;
import com.mico.xchange.chaoex.service.ChaoexMarketDataService;
import com.mico.xchange.chaoex.service.ChaoexTradeService;
import com.mico.xchange.utils.AuthUtils;
import com.mico.xchange.utils.nonce.AtomicLongCurrentTimeIncrementalNonceFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import si.mazi.rescu.SynchronizedValueFactory;

import java.text.DecimalFormat;

public class ChaoexLoginExchange extends BaseExchange {

  private static final Logger LOG = LoggerFactory.getLogger(ChaoexLoginExchange.class);

  private static final int DEFAULT_PRECISION = 8;

  private SynchronizedValueFactory<Long> nonceFactory =
      new AtomicLongCurrentTimeIncrementalNonceFactory();
  private Long deltaServerTimeExpire;
  private Long deltaServerTime;

  @Override
  protected void initServices() {
    this.marketDataService = new ChaoexMarketDataService(this);
    this.tradeService = new ChaoexTradeService(this);
    this.accountService = new ChaoexAccountService(this);
  }

  @Override
  public SynchronizedValueFactory<Long> getNonceFactory() {

    return nonceFactory;
  }

  @Override
  public ExchangeSpecification getDefaultExchangeSpecification() {
    ExchangeSpecification spec = new ExchangeSpecification(this.getClass().getCanonicalName());
    spec.setSslUri(ConfigUtils.getValue("chaoex.login.httpadd"));
    spec.setHost(ConfigUtils.getValue("chaoex.login.add"));

    //TODO 配置修改
//    spec.setSslUri("http://www.fuex.co/12lian");
//    spec.setHost("http://www.fuex.co");
    spec.setPort(80);
    spec.setExchangeName("Chaoex");
    spec.setExchangeDescription("Chao Exchange.");
    AuthUtils.setApiAndSecretKey(spec, "chaoex");
    return spec;
  }


  @Override
  public void remoteInit() {
    try {
      // populate currency pair keys only, exchange does not provide any other metadata for download
/*      Map<CurrencyPair, CurrencyPairMetaData> currencyPairs = exchangeMetaData.getCurrencyPairs();
      Map<Currency, CurrencyMetaData> currencies = exchangeMetaData.getCurrencies();*/

      ChaoexMarketDataService marketDataService =
          (ChaoexMarketDataService) this.marketDataService;

    } catch (Exception e) {
      logger.warn("An exception occurred while loading the metadata", e);
    }
  }

  private int numberOfDecimals(String value) {
    try {
      double d = Double.parseDouble(value);
      String s = new DecimalFormat("#.############").format(d);
      return s.split("\\.")[1].length();
    } catch (ArrayIndexOutOfBoundsException e) {
      return DEFAULT_PRECISION;
    }
  }

  public void clearDeltaServerTime() {
    deltaServerTime = null;
  }


}
