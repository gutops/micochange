package com.mico.xchange.chaoex.service;

import com.mico.micochange.common.MD5Util;
import com.mico.xchange.Exchange;
import com.mico.xchange.chaoex.dto.ChaoexException;
import com.mico.xchange.chaoex.dto.account.results.ChaoexAccountResult;
import com.mico.xchange.chaoex.dto.trade.results.ChaoexLoginResult;

import java.io.IOException;

public class ChaoexAccountServiceRaw extends ChaoexBaseService {

  public ChaoexAccountServiceRaw(Exchange exchange) {
    super(exchange);
  }

  public ChaoexAccountResult account(String uid,
                                     String token)
      throws ChaoexException, IOException {
    return chaoex.account(token,uid,"zh_CN", getTimestamp(), super.signatureCreator);
  }

  public ChaoexLoginResult login(String email,String pwd) throws ChaoexException, IOException{
    String md5Pwd = MD5Util.MD5Encode(pwd+"dig?F*ckDang5PaSsWOrd&%(12lian0160630).");
    return chaoex.login(email,md5Pwd, getTimestamp(), super.signatureCreator);
  }

}
