package com.mico.xchange.chaoex;

import com.mico.xchange.chaoex.dto.ChaoexException;
import com.mico.xchange.chaoex.dto.ChaoexResult;
import com.mico.xchange.chaoex.dto.ChaoexStringResult;
import com.mico.xchange.chaoex.dto.account.results.ChaoexAccountResult;
import com.mico.xchange.chaoex.dto.trade.*;
import com.mico.xchange.chaoex.dto.trade.results.ChaoexLoginResult;
import com.mico.xchange.chaoex.dto.trade.results.ChaoexOpenOrdersResult;
import si.mazi.rescu.ParamsDigest;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import java.io.IOException;
import java.math.BigDecimal;

@Path("/12lian/")
@Consumes(MediaType.APPLICATION_FORM_URLENCODED)
@Produces(MediaType.APPLICATION_JSON)
public interface ChaoexAuthenticated extends Chaoex {

  public static final String SIGNATURE = "sign";

  @POST
  @Path("order/order")
  /*
  *buyOrSell	买卖方向，1是buy，2是sell	int
  *currencyId	交易币种	int
  *baseCurrencyId	基础币种	int
  *fdPassword	交易密码,可以为空	string
  *num	数量，保留小数点后最多8位小数	decimal
  *price	价格，保留小数点后最多8位小数	decimal
  *source	程序化交易对接类型，该值目前为5	int
  *type	交易类型，该值目前为1	int
  *token	请求认证，如果token过期后需要重新获取	string
  *uid	用户id	int
  *local	语种，繁体中文,可选zh_TW、en	string
   *timestamp	时间戳
   */
  ChaoexStringResult newOrder(
          @FormParam("buyOrSell") String buyOrSell,
          @FormParam("currencyId") String currencyId,
          @FormParam("baseCurrencyId") String baseCurrencyId,
          @FormParam("fdPassword") String fdPassword,
          @FormParam("num") BigDecimal num,
          @FormParam("price") BigDecimal price,
          @FormParam("source") String source,
          @FormParam("type") String type,
          @FormParam("token") String token,
          @FormParam("uid") String uid,
          @FormParam("local") String local,
          @FormParam("timestamp") String timestamp,
          @QueryParam(SIGNATURE) ParamsDigest signature)
      throws IOException, ChaoexException;

  @POST
  @Path("user/signLogin")
  /*
  email	平台登录名	string
  pwd	平台登录密码	string
  timestamp	时间戳	string
  sign	签名	string
   */
  ChaoexLoginResult login(
          @FormParam("email") String email,
          @FormParam("pwd") String pwd,
          @FormParam("timestamp") String timestamp,
          @QueryParam(SIGNATURE) ParamsDigest signature)
      throws IOException, ChaoexException;

  @POST
  @Path("user/trOrderListByCustomer")
  /**
   *beginTime	查询开始日期，格式：2018-04-25	string
   * endTime	查询结束日期，格式：2018-04-26	string
   * start	查询起点，默认为1，最大为999	int
   * size	查询数量	int
   * status	委托单状态，未成交=0、部分成交=1、全部成交=2、撤单=4、全部状态=10	int
   * buyOrSell	买卖方向，0是全部方向，1是buy，2是sell	int
   * currencyId	交易币种id	int
   * baseCurrencyId	基础币种id	int
   * priceType	价格类型，默认为0，表示保留0位小数
   * token	请求认证，如果token过期后需要重新获取	string
   * uid	用户id	int
   * local	语种，繁体中文,可选zh_TW、en	string
   * timestamp	时间戳	string
   */
  ChaoexOpenOrdersResult orderStatus(
          @FormParam("beginTime") String beginTime,
          @FormParam("endTime") String endTime,
          @FormParam("start") int start,
          @FormParam("size") int size,
          @FormParam("status") int status,
          @FormParam("buyOrSell") int buyOrSell,
          @FormParam("currencyId") String currencyId,
          @FormParam("baseCurrencyId") String baseCurrencyId,
          @FormParam("priceType") int priceType,
          @FormParam("token") String token,
          @FormParam("uid") String uid,
          @FormParam("local") String local,
          @FormParam("timestamp") String timestamp,
          @QueryParam(SIGNATURE) ParamsDigest signature)
      throws IOException, ChaoexException;

  @POST
  @Path("order/cancel")
  /**
   *currencyId	基础币种id	int
   * orderNo	委托单号	string
   * fdPassword	交易密码	string
   * source	程序化交易对接类型，该值目前为1	int
   * token	请求认证，如果token过期后需要重新获取	string
   * uid	用户id	int
   * local	语种，繁体中文,可选zh_TW、en	string
   * timestamp	时间戳	string
   */
  ChaoexStringResult cancelOrder(
          @FormParam("currencyId") String currencyId,
          @FormParam("orderNo") String orderNo,
          @FormParam("fdPassword") String fdPassword,
          @FormParam("source") int source,
          @FormParam("token") String token,
          @FormParam("uid") String uid,
          @FormParam("local") String local,
          @FormParam("timestamp") String timestamp,
          @QueryParam(SIGNATURE) ParamsDigest signature)
      throws IOException, ChaoexException;

  @POST
  @Path("coin/customerCoinAccount")
  /**
   *token	请求认证，如果token过期后需要重新获取	string
   * uid	用户id	int
   * local	语种，繁体中文,可选zh_TW、en	string
   * timestamp	时间戳	string
   */
  ChaoexAccountResult account(
          @FormParam("token") String token,
          @FormParam("uid") String uid,
          @FormParam("local") String local,
          @FormParam("timestamp") String timestamp,
          @QueryParam(SIGNATURE) ParamsDigest signature)
      throws IOException, ChaoexException;

}
