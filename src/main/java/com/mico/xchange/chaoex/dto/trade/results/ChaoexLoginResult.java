package com.mico.xchange.chaoex.dto.trade.results;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.mico.xchange.chaoex.dto.ChaoexResult;
import com.mico.xchange.chaoex.dto.marketdata.ChaoexTicker;
import com.mico.xchange.chaoex.dto.trade.ChaoexLogin;

public class ChaoexLoginResult extends ChaoexResult<ChaoexLogin> {


  @JsonCreator
  public ChaoexLoginResult(
      @JsonProperty("status") String status,
      @JsonProperty("attachment") ChaoexLogin login,
      @JsonProperty("message") String errMsg) {
    super(status, errMsg, login);
  }

}
