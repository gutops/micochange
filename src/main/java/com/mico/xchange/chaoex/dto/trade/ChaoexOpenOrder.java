package com.mico.xchange.chaoex.dto.trade;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.List;

public final class ChaoexOpenOrder {

  private final String total;
  private final List<ChaoexOrder> chaoexOpenOrderList;
  /*
   *token	操作请求token	string
    uid	用户id	int
    uname	用户姓名，目前为空	string
    isShow	是否展示高级选项，0是不可以，1是可以	int
   */
  public ChaoexOpenOrder(
      @JsonProperty("total") String total,
      @JsonProperty("list") List<ChaoexOrder> list) {
    this.total = total;
    this.chaoexOpenOrderList = list;
  }

  public String getTotal() {
    return total;
  }

  public List<ChaoexOrder> getChaoexOpenOrderList() {
    return chaoexOpenOrderList;
  }

  @Override
  public String toString() {
    return "ChaoexOpenOrder{" +
            "total='" + total + '\'' +
            ", chaoexOpenOrderList=" + chaoexOpenOrderList +
            '}';
  }
}
