package com.mico.xchange.chaoex.dto;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;

public class ChaoexStringResult{

  private final String status;
  private final String errMsg;
  private final String result;

  @JsonCreator
  public ChaoexStringResult(
          @JsonProperty("status") String status,
          @JsonProperty("message") String errMsg,
          @JsonProperty("attachment") String result) {
    this.status = status;
    this.errMsg = errMsg;
    this.result = result;
  }

  public boolean isSuccess() {
    return getStatus().equals("200");
  }

  public String getStatus() {
    return status;
  }

  public String getError() {
    return errMsg;
  }

  public String getResult() {
    return result;
  }

  @Override
  public String toString() {
    return String.format(
            "ChaoexStringResult [%s, %s]", status, isSuccess() ? getResult().toString() : getError());
  }

}
