package com.mico.xchange.chaoex.dto.marketdata;

import com.fasterxml.jackson.annotation.JsonProperty;

public class ChaoexAssetPair {

  private final long id;
  private final String baseCurrencyId;
  private final String baseCurrencyName;
  private final String baseCurrency;
  private final String tradeCurrencyId;
  private final String tradeCurrencyName;
  private final String quoteCurrency;
  private final String minQty;
  private final String maxQty;

  public ChaoexAssetPair(
      @JsonProperty("id") long id,
      @JsonProperty("baseCurrencyId") String baseCurrencyId,
      @JsonProperty("baseCurrencyName") String baseCurrencyName,
      @JsonProperty("baseCurrencyNameEn") String baseCurrency,
      @JsonProperty("tradeCurrencyId") String tradeCurrencyId,
      @JsonProperty("tradeCurrencyName") String tradeCurrencyName,
      @JsonProperty("tradeCurrencyNameEn") String quoteCurrency,
      @JsonProperty("amountLowLimit") String amount_min,
      @JsonProperty("amountHighLimit") String amount_max) {
    this.id = id;
    this.baseCurrencyId = baseCurrencyId;
    this.baseCurrencyName = baseCurrencyName;
    this.baseCurrency = baseCurrency;
    this.tradeCurrencyId = tradeCurrencyId;
    this.tradeCurrencyName = tradeCurrencyName;
    this.quoteCurrency = quoteCurrency;
    this.minQty = amount_min;
    this.maxQty = amount_max;
  }

  public String getKey() {
    return baseCurrency + quoteCurrency;
  }

  public long getId() {
    return id;
  }

  public String getBaseCurrencyId() {
    return baseCurrencyId;
  }

  public String getBaseCurrencyName() {
    return baseCurrencyName;
  }

  public String getBaseCurrency() {
    return baseCurrency;
  }

  public String getTradeCurrencyId() {
    return tradeCurrencyId;
  }

  public String getTradeCurrencyName() {
    return tradeCurrencyName;
  }

  public String getQuoteCurrency() {
    return quoteCurrency;
  }

  public String getMinQty() {
    return minQty;
  }

  public String getMaxQty() {
    return maxQty;
  }

  @Override
  public String toString() {
    return "ChaoexAssetPair{" +
            "id=" + id +
            ", baseCurrencyId='" + baseCurrencyId + '\'' +
            ", baseCurrencyName='" + baseCurrencyName + '\'' +
            ", baseCurrency='" + baseCurrency + '\'' +
            ", tradeCurrencyId='" + tradeCurrencyId + '\'' +
            ", tradeCurrencyName='" + tradeCurrencyName + '\'' +
            ", quoteCurrency='" + quoteCurrency + '\'' +
            ", minQty='" + minQty + '\'' +
            ", maxQty='" + maxQty + '\'' +
            '}';
  }
}
