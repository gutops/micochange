package com.mico.xchange.chaoex.dto;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;

public class ChaoexResult<V> {

  private final String status;
  private final String errMsg;
  private final V result;

  @JsonCreator
  public ChaoexResult(
      @JsonProperty("status") String status,
      @JsonProperty("message") String errMsg,
      V result) {
    this.status = status;
    this.errMsg = errMsg;
    this.result = result;
  }

  public boolean isSuccess() {
    return getStatus().equals("200");
  }

  public String getStatus() {
    return status;
  }

  public String getError() {
    return errMsg;
  }

  public V getResult() {
    return result;
  }

  @Override
  public String toString() {
    return String.format(
        "ChaoexResult [%s, %s]", status, isSuccess() ? getResult().toString() : getError());
  }
}
