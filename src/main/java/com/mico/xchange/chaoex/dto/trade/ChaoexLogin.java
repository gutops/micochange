package com.mico.xchange.chaoex.dto.trade;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.math.BigDecimal;

public final class ChaoexLogin {

  private final String token;
  private final String uid;
  private final String uname;
  private final String isShow;
  /*
   *token	操作请求token	string
    uid	用户id	int
    uname	用户姓名，目前为空	string
    isShow	是否展示高级选项，0是不可以，1是可以	int
   */
  public ChaoexLogin(
      @JsonProperty("token") String token,
      @JsonProperty("uid") String uid,
      @JsonProperty("uname") String uname,
      @JsonProperty("isShow") String isShow) {
    this.token = token;
    this.uid = uid;
    this.uname = uname;
    this.isShow = isShow;
  }

  public String getToken() {
    return token;
  }

  public String getUid() {
    return uid;
  }

  public String getUname() {
    return uname;
  }

  public String getIsShow() {
    return isShow;
  }

  @Override
  public String toString() {
    return "ChaoexLogin{" +
            "token='" + token + '\'' +
            ", uid='" + uid + '\'' +
            ", uname='" + uname + '\'' +
            ", isShow='" + isShow + '\'' +
            '}';
  }
}
