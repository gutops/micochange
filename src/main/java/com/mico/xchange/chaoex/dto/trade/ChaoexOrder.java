package com.mico.xchange.chaoex.dto.trade;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.mico.xchange.dto.Order;

import java.math.BigDecimal;
import java.util.List;

public final class ChaoexOrder {

  private final BigDecimal finalDealPrice;
  private final String baseCurrencyId;
  private final String baseCurrencyName;
  private final String baseCurrencyNameEn;
  private final String buyOrSell;
  private final String currencyName;
  private final String currencyNameEn;
  private final BigDecimal averagePrice;
  private final BigDecimal fee;
  private final BigDecimal num;
  private final String id;
  private final String orderTime;
  private final BigDecimal price;
  private final BigDecimal remainNum;
  private final String status;
  private final BigDecimal cumulativeAmount;
  /*
   *averagePrice	平均委托金额	decimal
baseCurrencyId	基础币id	int
baseCurrencyName	基础币名称	string
baseCurrencyNameEn	基础币简称	string
buyOrSell	买卖方向，0是全部方向，1是buy，2是sell	int
currencyName	数字货币全称	string
currencyNameEn	数字货币简称	string
dealAmount	成交金额	decimal
fee	交易费用	decimal
num	委托数量	decimal
orderNo	委托单号	string
orderTime	委托时间	string
price	委托价格	decimal
remainNum	剩余数量	decimal
status	委托单状态，未成交=0、部分成交=1、全部成交=2、撤单=4、全部状态=10	int
tradeNum	成交数量	decimal
   */
  public ChaoexOrder(
      @JsonProperty("averagePrice") BigDecimal averagePrice,
      @JsonProperty("baseCurrencyId") String baseCurrencyId,
      @JsonProperty("baseCurrencyName") String baseCurrencyName,
      @JsonProperty("baseCurrencyNameEn") String baseCurrencyNameEn,
      @JsonProperty("buyOrSell") String buyOrSell,
      @JsonProperty("currencyName") String currencyName,
      @JsonProperty("currencyNameEn") String currencyNameEn,
      @JsonProperty("dealAmount") BigDecimal dealAmount,
      @JsonProperty("fee") BigDecimal fee,
      @JsonProperty("num") BigDecimal num,
      @JsonProperty("orderNo") String orderNo,
      @JsonProperty("orderTime") String orderTime,
      @JsonProperty("price") BigDecimal price,
      @JsonProperty("remainNum") BigDecimal remainNum,
      @JsonProperty("status") String status,
      @JsonProperty("tradeNum") BigDecimal tradeNum) {
    this.finalDealPrice = dealAmount;
    this.baseCurrencyId = baseCurrencyId;
    this.baseCurrencyName = baseCurrencyName;
    this.baseCurrencyNameEn = baseCurrencyNameEn;
    this.buyOrSell = buyOrSell;
    this.currencyName = currencyName;
    this.currencyNameEn = currencyNameEn;
    this.averagePrice = averagePrice;
    this.fee =fee;
    this.num = num;
    this.id = orderNo;
    this.orderTime = orderTime;
    this.price = price;
    this.remainNum = remainNum;
    this.status =status;
    this.cumulativeAmount = tradeNum;
  }

  public String getBaseCurrencyId() {
    return baseCurrencyId;
  }

  public String getBaseCurrencyName() {
    return baseCurrencyName;
  }

  public String getBaseCurrencyNameEn() {
    return baseCurrencyNameEn;
  }

  public String getBuyOrSell() {
    return buyOrSell;
  }

  public String getCurrencyName() {
    return currencyName;
  }

  public String getCurrencyNameEn() {
    return currencyNameEn;
  }

  public BigDecimal getAveragePrice() {
    return averagePrice;
  }

  public BigDecimal getFee() {
    return fee;
  }

  public BigDecimal getNum() {
    return num;
  }



  public String getOrderTime() {
    return orderTime;
  }

  public BigDecimal getPrice() {
    return price;
  }

  public BigDecimal getRemainNum() {
    return remainNum;
  }

  public Order.OrderStatus getStatus() {
    if("0".equals(status)){
      return Order.OrderStatus.NEW;
    }else if("1".equals(status)){
      return Order.OrderStatus.PARTIALLY_FILLED;
    }else if("2".equals(status)){
      return Order.OrderStatus.FILLED;
    }else if("4".equals(status)){
      return Order.OrderStatus.CANCELED;
    }else if("5".equals(status)){
      return Order.OrderStatus.PARTIALLY_CANCELED;
    }
    return Order.OrderStatus.NEW;
  }

  public BigDecimal getFinalDealPrice() {
    return finalDealPrice;
  }

  public BigDecimal getCumulativeAmount() {
    return cumulativeAmount;
  }

  @Override
  public String toString() {
    return "ChaoexOrder{" +
            "finalDealPrice=" + finalDealPrice +
            ", baseCurrencyId='" + baseCurrencyId + '\'' +
            ", baseCurrencyName='" + baseCurrencyName + '\'' +
            ", baseCurrencyNameEn='" + baseCurrencyNameEn + '\'' +
            ", buyOrSell='" + buyOrSell + '\'' +
            ", currencyName='" + currencyName + '\'' +
            ", currencyNameEn='" + currencyNameEn + '\'' +
            ", averagePrice=" + averagePrice +
            ", fee=" + fee +
            ", num=" + num +
            ", orderNo='" + id + '\'' +
            ", orderTime='" + orderTime + '\'' +
            ", price=" + price +
            ", remainNum=" + remainNum +
            ", status='" + status + '\'' +
            ", cumulativeAmount=" + cumulativeAmount +
            '}';
  }

  public String getId() {
    return id;
  }
}
