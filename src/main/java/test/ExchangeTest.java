package test;

import com.alibaba.fastjson.JSONObject;
import com.mico.xchange.Exchange;
import com.mico.xchange.ExchangeFactory;
import com.mico.xchange.binance.BinanceExchange;
import com.mico.xchange.binance.dto.marketdata.BinanceKline;
import com.mico.xchange.binance.dto.marketdata.KlineInterval;
import com.mico.xchange.binance.service.BinanceAccountService;
import com.mico.xchange.binance.service.BinanceMarketDataService;
import com.mico.xchange.binance.service.BinanceTradeService;
import com.mico.xchange.currency.Currency;
import com.mico.xchange.currency.CurrencyPair;
import com.mico.xchange.dto.Order;
import com.mico.xchange.dto.account.AccountInfo;
import com.mico.xchange.dto.account.Balance;
import com.mico.xchange.dto.account.FundingRecord;
import com.mico.xchange.dto.marketdata.Ticker;
import com.mico.xchange.dto.trade.LimitOrder;
import com.mico.xchange.dto.trade.MarketOrder;
import com.mico.xchange.dto.trade.OpenOrders;
import com.mico.xchange.dto.trade.UserTrades;
import com.mico.xchange.huobi.HuobiExchange;
import com.mico.xchange.huobi.service.HuobiAccountService;
import com.mico.xchange.huobi.service.HuobiTradeService;
import com.mico.xchange.service.trade.params.TradeHistoryParams;

import java.io.IOException;
import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import java.util.*;


public class ExchangeTest {


    /**
     * @param args
     */
    public static void main(String[] args) {
			/*String getOrderStr = "{\"result\":[{\"averagePrice\":0,\"cumulativeAmount\":0.0,\"currencyPair\":{\"base\":{\"commonlyUsedCurrency\":{\"$ref\":\"@\"},\"currencyCode\":\"ETH\",\"currencyCodes\":[\"ETH\"],\"displayName\":\"Ether\",\"iso4217Currency\":{\"$ref\":\"@\"},\"symbol\":\"ETH\"},\"counter\":{\"commonlyUsedCurrency\":{\"$ref\":\"@\"},\"currencyCode\":\"USDT\",\"currencyCodes\":[\"USDT\"],\"displayName\":\"Tether USD Anchor\",\"iso4217Currency\":{\"$ref\":\"@\"},\"symbol\":\"USDT\"}},\"fee\":0,\"id\":\"3834506596\",\"limitPrice\":100000.000000000000000000,\"orderFlags\":[],\"originalAmount\":0.010000000000000000,\"remainingAmount\":0.010000000000000000,\"status\":\"CANCELED\",\"timestamp\":1524927975693,\"type\":\"ASK\"}]}";
			JSONObject jsonObject = JSONObject.parseObject(getOrderStr);
			System.out.println(jsonObject.getJSONArray("result").getJSONObject(0).getString("status"));*/
        String getTicket = "{\"ask\":9139.21000000,\"bid\":9139.20000000,\"currencyPair\":{\"base\":{\"commonlyUsedCurrency\":{\"$ref\":\"@\"},\"currencyCode\":\"BTC\",\"currencyCodes\":[\"BTC\",\"XBT\"],\"displayName\":\"Bitcoin\",\"iso4217Currency\":{\"commonlyUsedCurrency\":{\"$ref\":\"..\"},\"currencyCode\":\"XBT\",\"currencyCodes\":[\"BTC\",\"XBT\"],\"displayName\":\"Bitcoin\",\"iso4217Currency\":{\"$ref\":\"@\"},\"symbol\":\"BTC\"},\"symbol\":\"BTC\"},\"counter\":{\"commonlyUsedCurrency\":{\"$ref\":\"@\"},\"currencyCode\":\"USDT\",\"currencyCodes\":[\"USDT\"],\"displayName\":\"Tether USD Anchor\",\"iso4217Currency\":{\"$ref\":\"@\"},\"symbol\":\"USDT\"}},\"high\":9343.00000000,\"last\":9139.20000000,\"low\":8965.00000000,\"open\":9283.00000000,\"quoteVolume\":258520548.3528192000000000,\"volume\":28286.99977600,\"vwap\":9136.20350970}\n";
        bian();
    }

    private static void huobi() {
        Exchange exchange = ExchangeFactory.INSTANCE.createExchange(HuobiExchange.class.getName(), "a21ee8ee-ad4a91ea-8c100247-fc998", "72252d83-beeb221a-da2c8005-653c4");
        HuobiAccountService accountService = (HuobiAccountService) exchange.getAccountService();
        try {
            //查询所有交易对余额
									/*AccountInfo acc = accountService.getAccountInfo();
									Map<Currency,Balance> balanceMap = acc.getWallet().getBalances();
									Map<Currency,Balance> retMap = new HashMap<Currency,Balance>();
									for(Currency currency : balanceMap.keySet()){
										if(balanceMap.get(currency).getTotal().compareTo(new BigDecimal(0))>0){
											retMap.put(currency,balanceMap.get(currency));
										}
									}
									System.out.println("sdfsdf");*/
        } catch (Exception e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }


        //交易信息
        HuobiTradeService tradeService = (HuobiTradeService) exchange.getTradeService();
        String orderId = "3834506596";
        try {
            //订单撤销
            //				boolean flag = tradeService.cancelOrder(orderId);
            //所有挂单
//									OpenOrders openOrders = tradeService.getOpenOrders();
            Map<String, Object> retMap = new HashMap<String, Object>();
            Collection<Order> lst = tradeService.getOrder(orderId);
            retMap.put("result", lst);
            System.out.println("#####" + JSONObject.toJSONString(retMap));
        } catch (IOException e) {
            e.printStackTrace();
        }

    }


    /*
     *
     *
     *虚拟币安全
     *
     */
    private static void bian() {
        Exchange exchange = ExchangeFactory.INSTANCE.createExchange(BinanceExchange.class.getName(), "8bcYzuqRdJnyaldjOQcGiU6Ew7M6IBSjbmYT42jpvF7qt8PPxlebwimfDyuQjdOq", "tM9vEtv4lOwjJqo3almZVOVSTdxPWjO1LhFL9buyZrwE0AaN8ejqHm3IFm5aXVxn");
        //账户信息
					/*BinanceAccountService accountService = (BinanceAccountService) exchange.getAccountService();
					try {
						//账户查询
						AccountInfo acc = accountService.getAccountInfo();
						Map<Currency,Balance> balanceMap = acc.getWallet().getBalances();
						Map<Currency,Balance> retMap = new HashMap<Currency,Balance>();
						for(Currency currency : balanceMap.keySet()){
							if(balanceMap.get(currency).getTotal().compareTo(new BigDecimal(0))>0){
								retMap.put(currency,balanceMap.get(currency));
							}
						}
						//取消订单历史
						TradeHistoryParams param = accountService.createFundingHistoryParams();
						List<FundingRecord>  list = accountService.getFundingHistory(param);

					} catch (Exception e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}*/
        //市场信息
        CurrencyPair pair = new CurrencyPair("BTC", "USDT");
        BinanceMarketDataService marketService = (BinanceMarketDataService) exchange.getMarketDataService();
//	    marketService.aggTrades(pair, fromId, startTime, endTime, limit);
//	    marketService.getBinanceOrderbook(pair, limit);
        try {
            Ticker ticker = marketService.getTicker(pair);
            System.out.println(JSONObject.toJSONString(ticker));
//				List<BinanceKline> binanceKlines = marketService.klines(pair, KlineInterval.d1);
        } catch (IOException e) {
            e.printStackTrace();
        }
		/*marketService.getTickers(params);
	    marketService.getTrades(pair, args);
	    marketService.klines(pair, interval);
	    marketService.klines(pair, interval, limit, startTime, endTime);
	    marketService.lastKline(pair, interval);
	    marketService.ticker24h();
	    marketService.ticker24h(pair);
	    marketService.tickerAllBookTickers();
	    marketService.tickerAllPrices();
	    marketService.tickerPrice(pair);
	    marketService.verifyOrder(limitOrder);
	    marketService.verifyOrder(marketOrder);*/
        //交易信息
	   /* ChaoexTradeService tradeService = (ChaoexTradeService)exchange.getTradeService();
			try {
				//订单撤销
				String orderId = "";
				boolean flag = tradeService.cancelOrder(orderId);
				tradeService.getOpenOrders();
				LimitOrder limitOrder=new LimitOrder(Order.OrderType.BID,new BigDecimal("0.0001"),new CurrencyPair("BTC","USDT"), UUID.randomUUID().toString(),new Date(),new BigDecimal("100000"));
				tradeService.verifyOrder(limitOrder);
				String resultLimit = tradeService.placeLimitOrder(limitOrder);
				MarketOrder marketOrder = new MarketOrder(Order.OrderType.BID,new BigDecimal("0.0001"),new CurrencyPair("BTC","USDT"), UUID.randomUUID().toString(),new Date(),new BigDecimal("100000"),new BigDecimal("100000"),new BigDecimal("100000"), Order.OrderStatus.NEW);
				tradeService.verifyOrder(marketOrder);
				String resultMarket = tradeService.placeMarketOrder(marketOrder);
				tradeService.createOpenOrdersParams();
				tradeService.createTradeHistoryParams();
			} catch (IOException e) {
				e.printStackTrace();
			}*/
			/*tradeService.getOpenOrders(pair);
			tradeService.allOrders(pair, orderId, limit, recvWindow, timestamp);
			tradeService.cancelOrder(params);
			tradeService.cancelOrder(pair, orderId, origClientOrderId, newClientOrderId, recvWindow, timestamp);
			tradeService.closeDataStream(listenKey);
	    tradeService.getOpenOrders(params);
	    tradeService.getOrder(arg0);
	    tradeService.getOrder(orderIds);
	    tradeService.getTradeHistory(arg0);
	    tradeService.keepAliveDataStream(listenKey);
	    tradeService.myTrades(pair, limit, fromId, recvWindow, timestamp);
	    tradeService.newOrder(pair, side, type, timeInForce, quantity, price, newClientOrderId, stopPrice, icebergQty, recvWindow, timestamp);
	    tradeService.openOrders(recvWindow, timestamp);
	    tradeService.openOrders(pair, recvWindow, timestamp);
	    tradeService.orderStatus(pair, orderId, origClientOrderId, recvWindow, timestamp);
	    tradeService.placeLimitOrder(arg0);
	    tradeService.placeMarketOrder(mo);
	    tradeService.placeStopOrder(arg0);
	    tradeService.placeTestOrder(type, order, limitPrice, stopPrice);
	    tradeService.startUserDataStream();
	    tradeService.testNewOrder(pair, side, type, timeInForce, quantity, price, newClientOrderId, stopPrice, icebergQty, recvWindow, timestamp);
	    tradeService.verifyOrder(limitOrder);
	    tradeService.verifyOrder(marketOrder);*/
    }

}

