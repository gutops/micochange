package test;


import com.mico.micochange.common.RSACoder;
import org.java_websocket.client.WebSocketClient;

import javax.crypto.KeyGenerator;
import javax.crypto.Mac;
import javax.crypto.SecretKey;
import javax.crypto.spec.SecretKeySpec;
import javax.net.ssl.SSLContext;
import javax.net.ssl.TrustManager;
import javax.net.ssl.X509TrustManager;
import javax.xml.bind.annotation.adapters.HexBinaryAdapter;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.math.BigDecimal;
import java.net.URISyntaxException;
import java.security.NoSuchAlgorithmException;
import java.security.cert.CertificateException;
import java.security.cert.X509Certificate;
import java.sql.*;
import java.sql.Date;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;


/**
 * Created by acer on 2018/4/17.
 */
public class Test {
    /**
     * 产生HmacMD5摘要算法的密钥
     */
    public static byte[] initHmacMD5Key() throws NoSuchAlgorithmException {
        // 初始化HmacMD5摘要算法的密钥产生器
        KeyGenerator generator = KeyGenerator.getInstance("HmacMD5");
        // 产生密钥
        SecretKey secretKey = generator.generateKey();
        // 获得密钥
        byte[] key = secretKey.getEncoded();
        return key;
    }

    /**
     * HmacMd5摘要算法
     * 对于给定生成的不同密钥，得到的摘要消息会不同，所以在实际应用中，要保存我们的密钥
     */
    public static String encodeHmacMD5(byte[] data, byte[] key) throws Exception {
        // 还原密钥
        SecretKey secretKey = new SecretKeySpec(key, "HmacMD5");
        // 实例化Mac
        Mac mac = Mac.getInstance(secretKey.getAlgorithm());
        //初始化mac
        mac.init(secretKey);
        //执行消息摘要
        byte[] digest = mac.doFinal(data);
        return new HexBinaryAdapter().marshal(digest);//转为十六进制的字符串
    }


    /**
     * 产生HmacSHA1摘要算法的密钥
     */
    public static byte[] initHmacSHAKey() throws NoSuchAlgorithmException {
        // 初始化HmacMD5摘要算法的密钥产生器
        KeyGenerator generator = KeyGenerator.getInstance("HmacSHA1");
        // 产生密钥
        SecretKey secretKey = generator.generateKey();
        // 获得密钥
        byte[] key = secretKey.getEncoded();
        return key;
    }

    /**
     * HmacSHA1摘要算法
     * 对于给定生成的不同密钥，得到的摘要消息会不同，所以在实际应用中，要保存我们的密钥
     */
    public static String encodeHmacSHA(byte[] data, byte[] key) throws Exception {
        // 还原密钥
        SecretKey secretKey = new SecretKeySpec(key, "HmacSHA1");
        // 实例化Mac
        Mac mac = Mac.getInstance(secretKey.getAlgorithm());
        //初始化mac
        mac.init(secretKey);
        //执行消息摘要
        byte[] digest = mac.doFinal(data);
        return new HexBinaryAdapter().marshal(digest);//转为十六进制的字符串
    }


    /**
     * 产生HmacSHA256摘要算法的密钥
     */
    public static byte[] initHmacSHA256Key() throws NoSuchAlgorithmException {
        // 初始化HmacMD5摘要算法的密钥产生器
        KeyGenerator generator = KeyGenerator.getInstance("HmacSHA256");
        // 产生密钥
        SecretKey secretKey = generator.generateKey();
        // 获得密钥
        byte[] key = secretKey.getEncoded();
        return key;
    }

    /**
     * HmacSHA1摘要算法
     * 对于给定生成的不同密钥，得到的摘要消息会不同，所以在实际应用中，要保存我们的密钥
     */
    public static String encodeHmacSHA256(byte[] data, byte[] key) throws Exception {
        // 还原密钥
        SecretKey secretKey = new SecretKeySpec(key, "HmacSHA256");
        // 实例化Mac
        Mac mac = Mac.getInstance(secretKey.getAlgorithm());
        //初始化mac
        mac.init(secretKey);
        //执行消息摘要
        byte[] digest = mac.doFinal(data);
        System.out.println(digest.toString() + "             pp");
        return new HexBinaryAdapter().marshal(digest);//转为十六进制的字符串
    }


    /**
     * 产生HmacSHA256摘要算法的密钥
     */
    public static byte[] initHmacSHA384Key() throws NoSuchAlgorithmException {
        // 初始化HmacMD5摘要算法的密钥产生器
        KeyGenerator generator = KeyGenerator.getInstance("HmacSHA384");
        // 产生密钥
        SecretKey secretKey = generator.generateKey();
        // 获得密钥
        byte[] key = secretKey.getEncoded();
        return key;
    }

    /**
     * HmacSHA1摘要算法
     * 对于给定生成的不同密钥，得到的摘要消息会不同，所以在实际应用中，要保存我们的密钥
     */
    public static String encodeHmacSHA384(byte[] data, byte[] key) throws Exception {
        // 还原密钥
        SecretKey secretKey = new SecretKeySpec(key, "HmacSHA384");
        // 实例化Mac
        Mac mac = Mac.getInstance(secretKey.getAlgorithm());
        //初始化mac
        mac.init(secretKey);
        //执行消息摘要
        byte[] digest = mac.doFinal(data);
        return new HexBinaryAdapter().marshal(digest);//转为十六进制的字符串
    }


    /**
     * 产生HmacSHA256摘要算法的密钥
     */
    public static byte[] initHmacSHA512Key() throws NoSuchAlgorithmException {
        // 初始化HmacMD5摘要算法的密钥产生器
        KeyGenerator generator = KeyGenerator.getInstance("HmacSHA512");
        // 产生密钥
        SecretKey secretKey = generator.generateKey();
        // 获得密钥
        byte[] key = secretKey.getEncoded();
        return key;
    }

    /**
     * HmacSHA1摘要算法
     * 对于给定生成的不同密钥，得到的摘要消息会不同，所以在实际应用中，要保存我们的密钥
     */
    public static String encodeHmacSHA512(byte[] data, byte[] key) throws Exception {
        // 还原密钥
        SecretKey secretKey = new SecretKeySpec(key, "HmacSHA512");
        // 实例化Mac
        Mac mac = Mac.getInstance(secretKey.getAlgorithm());
        //初始化mac
        mac.init(secretKey);
        //执行消息摘要
        byte[] digest = mac.doFinal(data);
        return new HexBinaryAdapter().marshal(digest);//转为十六进制的字符串
    }

    public static void main(String[] args) throws Exception {
        /*String testString = "email=shutao_chen@126.com&pwd=e7b991317ad870770db9b69e97578548&timestamp=1528336976703";

        byte[] keyHmacSHA256=Test.initHmacSHA256Key();

        System.out.println(Test.encodeHmacSHA256(testString.getBytes(),keyHmacSHA256));


        System.out.println(keyHmacSHA256.toString());

        String hex = "25c695a2d1e0aee6297210c285b987c7e9ac6dee0469a1e7d139c753357b0a70";
        String chaoex = "739deeac15a533508d679c6bc7f67d29e92fce7f05359a500abd8fd9844134d97d60e76495850ba392694dd8a50fc6202d8b004f9b9d61422ae698d2e9fc07aeaf29feb02e0019dfa6f30fd2de03be8f2e4cf5c8442d1b56b560427dd1637934750e5c071e8c9928fa81bfb7de83001e0e70b2c774be4bf43fdaa5e932849c9e";
        System.out.println(hex);*/

        /*
        byte[] keyHmacMD5=Test.initHmacMD5Key();
        System.out.println(Test.encodeHmacMD5(testString.getBytes(),keyHmacMD5));

        byte[] keyHmacSHA1=Test.initHmacSHAKey();
        System.out.println(Test.encodeHmacSHA(testString.getBytes(),keyHmacSHA1));

        byte[] keyHmacSHA256=Test.initHmacSHA256Key();
        System.out.println(Test.encodeHmacSHA256(testString.getBytes(),keyHmacSHA256));

        byte[] keyHmacSHA384=Test.initHmacSHA384Key();
        System.out.println(Test.encodeHmacSHA384(testString.getBytes(),keyHmacSHA384));

        byte[] keyHmacSHA512=Test.initHmacSHA512Key();
        System.out.println(Test.encodeHmacSHA512(testString.getBytes(),keyHmacSHA512));*/

       /* for(int i=0;i<500;i++){
            MinaClientThread thread = new MinaClientThread();
            thread.setMsg("send msg "+ i +" result");
            thread.start();
        }*/
      /* String test = "1.0100";
        String var = test.substring(test.indexOf(".")+1);
        if(var.indexOf("1")>=0){
            System.out.println(var.indexOf("1")+1);
        }else{
            System.out.println("0");
        }*/


      /*//redis test
        RedisPool.getJedis().set("name","陈浩宇");
        System.out.println(RedisPool.getJedis().get("name"));
        System.out.println(RedisPool.getJedis().get("price:ba:EOS/USDT"));*/

        /*try {
//            MsgWebSocketClient wsc = new MsgWebSocketClient("wss://api.huobi.pro/ws");
            MsgWebSocketClient wsc = new MsgWebSocketClient("ws://121.40.165.18:8800");
                    // WebSocket连接wss链接
                    // This part is needed in case you are going to use self-signed
                    // certificates
                    TrustManager[] trustAllCerts = new TrustManager[] { new X509TrustManager() {
                public java.security.cert.X509Certificate[] getAcceptedIssuers() {
                    return new java.security.cert.X509Certificate[] {};
                }

                public void checkClientTrusted(X509Certificate[] chain,
                                               String authType) throws CertificateException {
                }

                public void checkServerTrusted(X509Certificate[] chain,
                                               String authType) throws CertificateException {
                }
            } };
            SSLContext sc = SSLContext.getInstance("TLS");
            sc.init(null, trustAllCerts, new java.security.SecureRandom());
            // Otherwise the line below is all that is needed.
             sc.init(null, null, null);
            wsc.setWebSocketFactory(new DefaultSSLWebSocketClientFactory(sc));
            WebClientEnum.CLIENT.initClient(wsc);
        } catch (URISyntaxException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
            System.out.println(e.getMessage());
        }*/
/*
String[] ids = "sdfsf".split("//|");
for(String id : ids) {
    System.out.println(id);
}*/
        String str1 = RSACoder.encryptBASE64("abcdefg".getBytes());
        String str3 = Base64.getUrlEncoder().encodeToString("abcdefg".getBytes());
        System.out.println(str1);
        System.out.println(str3);
        byte[] str2 = RSACoder.decryptBASE64(str1);
        byte[] str4 = Base64.getUrlDecoder().decode(str1);
        System.out.println(str2);
        System.out.println(str4);


    }
}
