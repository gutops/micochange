package test;

import ch.qos.logback.core.net.SyslogOutputStream;
import com.alibaba.fastjson.JSONObject;
import com.mico.xchange.Exchange;
import com.mico.xchange.ExchangeFactory;
import com.mico.xchange.binance.BinanceExchange;
import com.mico.xchange.binance.dto.marketdata.BinanceKline;
import com.mico.xchange.binance.dto.marketdata.BinanceOrderbook;
import com.mico.xchange.binance.dto.marketdata.KlineInterval;
import com.mico.xchange.binance.service.BinanceAccountService;
import com.mico.xchange.binance.service.BinanceMarketDataService;
import com.mico.xchange.binance.service.BinanceTradeHistoryParams;
import com.mico.xchange.binance.service.BinanceTradeService;
import com.mico.xchange.bitz.BitZExchange;
import com.mico.xchange.bitz.dto.marketdata.result.BitZTickerAllResult;
import com.mico.xchange.bitz.dto.trade.result.BitZBalanceResult;
import com.mico.xchange.bitz.service.BitZAccountDataService;
import com.mico.xchange.bitz.service.BitZMarketDataService;
import com.mico.xchange.chaoex.ChaoexExchange;
import com.mico.xchange.chaoex.dto.marketdata.ChaoexAssetPair;
import com.mico.xchange.chaoex.dto.marketdata.ChaoexTicker;
import com.mico.xchange.chaoex.dto.trade.results.ChaoexLoginResult;
import com.mico.xchange.chaoex.service.ChaoexAccountServiceRaw;
import com.mico.xchange.chaoex.service.ChaoexMarketDataServiceRaw;
import com.mico.xchange.currency.Currency;
import com.mico.xchange.currency.CurrencyPair;
import com.mico.xchange.dto.Order;
import com.mico.xchange.dto.account.AccountInfo;
import com.mico.xchange.dto.account.Balance;
import com.mico.xchange.dto.account.FundingRecord;
import com.mico.xchange.dto.marketdata.OrderBook;
import com.mico.xchange.dto.marketdata.Ticker;
import com.mico.xchange.dto.marketdata.Trade;
import com.mico.xchange.dto.marketdata.Trades;
import com.mico.xchange.dto.trade.*;
import com.mico.xchange.huobi.HuobiExchange;
import com.mico.xchange.huobi.dto.account.HuobiAccount;
import com.mico.xchange.huobi.dto.marketdata.HuobiAsset;
import com.mico.xchange.huobi.dto.marketdata.HuobiAssetPair;
import com.mico.xchange.huobi.dto.marketdata.HuobiDepth;
import com.mico.xchange.huobi.dto.marketdata.HuobiTicker;
import com.mico.xchange.huobi.service.HuobiAccountService;
import com.mico.xchange.huobi.service.HuobiMarketDataService;
import com.mico.xchange.huobi.service.HuobiTradeService;
import com.mico.xchange.service.trade.params.DefaultTradeHistoryParamsTimeSpan;
import com.mico.xchange.service.trade.params.TradeHistoryParamCurrencyPair;
import com.mico.xchange.service.trade.params.TradeHistoryParams;
import com.mico.xchange.service.trade.params.orders.OpenOrdersParams;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.math.BigDecimal;
import java.net.ServerSocket;
import java.net.Socket;
import java.text.SimpleDateFormat;
import java.util.*;


public class HuobiTest {


    /**
     * @param args
     */
    public static void main(String[] args) {
//			huobi();
//			bian();
//			bitz();
        chaoex();
    }

    private static void chaoex() {

        String private_key = "MIICdgIBADANBgkqhkiG9w0BAQEFAASCAmAwggJcAgEAAoGBALL4MuHiRM8Q6Yjj\n" +
                "RSLQsbfH9F/qqOXHZmujI0PkcN2f1o9tkLqOQiE1DV5B4C+p8QuKBJU0z6CFiFd6\n" +
                "qoK+I2LV11pdb46ukFgCQ1MOp4gjKUOZPchx5GqELRBKTwat+uE6Itv0jAEl5JmJ\n" +
                "hEx4W5Xud3Uip2nrngDuzX2eue/7AgMBAAECgYEAqwilX9ekWJraA/bmMGJvJKpT\n" +
                "sYKJjKZmDAGpBw3+it5g50sXICIpsqCHuQeNnx9ye8uXm5wPvGFArhFNeIsLNqNF\n" +
                "j5Dk1J5XWITTrWm3fM0dAxd0+ua//12UfzxeOWb5BoLEO6joyqAnjVCGuf3ALzLT\n" +
                "kDAFZ5/E3TJS3Aq0BFECQQDqZdVlpEEXzKpu2pI+Xp7IYiyo1+sBILQ+lJD/BRIF\n" +
                "D+6Avq0thc0ze2yQBdnbGF/XPkGy6qc8SHA5iO2Slj5zAkEAw3akQWDq2R2lp1a1\n" +
                "1D3qCvzYwD1O2wetZ2MWZye2TiRui67Aie5+XfVp4IP1pKfvG7qQY/aO6J8n0dfE\n" +
                "GDBeWQJAehuUwL+BvYE8ZEVvHBi0xRUHyOm6njrISzWgF4ovqf1ztRQgKW/jx2cL\n" +
                "1jeGF3IqM3pWRXuipW/jLsXbZZrpawJADApR4ekbblZiLuWre7B4q0aZ/3WHVvyp\n" +
                "FNZIuf/7PeMGoDoaiCSVyink5LycNBFYLEgYvt+gO/oZ1NZKsQbBuQJACR/jdG0A\n" +
                "5cmX8m3sKEcDhYGZT3kOLC3E6Dqt0e69+E6EED/azp874XeREgPymPS3yM5f8OWw\n" +
                "Xe4F45iePK6PXw==";

        String pub_key = "MIGfMA0GCSqGSIb3DQEBAQUAA4GNADCBiQKBgQCy+DLh4kTPEOmI40Ui0LG3x/Rf6qjlx2ZroyND5HDdn9aPbZC6jkIhNQ1eQeAvqfELigSVNM+ghYhXeqqCviNi1ddaXW+OrpBYAkNTDqeIIylDmT3IceRqhC0QSk8GrfrhOiLb9IwBJeSZiYRMeFuV7nd1Iqdp654A7s19nrnv+wIDAQAB";
        Exchange exchange = ExchangeFactory.INSTANCE.createExchange(ChaoexExchange.class.getName(), private_key, private_key);
        ChaoexMarketDataServiceRaw marketDataServiceRaw = (ChaoexMarketDataServiceRaw) exchange.getMarketDataService();

        ChaoexAccountServiceRaw chaoexAccountService = (ChaoexAccountServiceRaw) exchange.getAccountService();
        try {
				/*ChaoexAssetPair[] chaoexAssetPairs = marketDataServiceRaw.getAssetPairs();
				System.out.println("$$$$$$"+JSONObject.toJSONString(chaoexAssetPairs));

				ChaoexTicker chaoexTicker = marketDataServiceRaw.getTicket("BTC","ETH");
				System.out.println("$$$$$$"+JSONObject.toJSONString(chaoexTicker));*/

            ChaoexLoginResult result = chaoexAccountService.login("shutao_chen@126.com", "2343awCk");
            System.out.println("$$$$$$" + JSONObject.toJSONString(result));
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private static void bitz() {

        try {
            Exchange exchange = ExchangeFactory.INSTANCE.createExchange(BitZExchange.class.getName(), "958f94d68c7873109740914407491fa1", "dmIFIZ8GtIMwueiLZrCtMYxLkwRaz0f0E7EILMwQyg8tvQkj8UVULrUAfkf4I4jm");

            BitZAccountDataService accountDataService = (BitZAccountDataService) exchange.getAccountService();
            BitZBalanceResult result = accountDataService.getBalances();

            System.out.println(result.getData().getTotal());
				/*BitZMarketDataService marketDataService = (BitZMarketDataService)exchange.getMarketDataService();
				CurrencyPair currencyPair = new CurrencyPair("ETH","BTC");

					Ticker ticker = marketDataService.getTicker(currencyPair);
					System.out.println(ticker.getAsk());
				*/
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    private static void huobi() {
        Exchange exchange = ExchangeFactory.INSTANCE.createExchange(HuobiExchange.class.getName(), "a21ee8ee-ad4a91ea-8c100247-fc998", "72252d83-beeb221a-da2c8005-653c4");
        HuobiAccountService accountService = (HuobiAccountService) exchange.getAccountService();
        try {
            //查询所有交易对余额
            AccountInfo acc = accountService.getHadaxAccountInfo();
            String str = acc.getUsername();
            System.out.println(str);
            Map<Currency, Balance> balanceMap = acc.getWallet().getBalances();
            Map<String, Balance> retMap = new HashMap<String, Balance>();
            for (Currency currency : balanceMap.keySet()) {
                if (balanceMap.get(currency).getTotal().compareTo(new BigDecimal(0)) > 0) {
                    Balance balance = balanceMap.get(currency);
                    balance.setCurrency(null);
                    retMap.put(currency.getCurrencyCode(), balance);
                }
            }

				/*System.out.println(JSONObject.toJSONString(retMap));
				//余额、持仓、冻结、历史交易

				HuobiTradeService tradeService = (HuobiTradeService)exchange.getTradeService();
				OpenOrdersParams openOrdersParams  = tradeService.createOpenOrdersParams();
				OpenOrders openOrders = tradeService.getOpenOrders(openOrdersParams);
				System.out.println(JSONObject.toJSONString(openOrders));
				DefaultTradeHistoryParamsTimeSpan tradeHistoryParams = new DefaultTradeHistoryParamsTimeSpan();
				Calendar calendar= Calendar.getInstance();
				calendar.add(Calendar.DAY_OF_YEAR,-1);
				tradeHistoryParams.setStartTime(new Date(calendar.getTime().getTime()));
				tradeHistoryParams.setStartTime(new Date());
				UserTrades trades =  tradeService.getTradeHistory(tradeHistoryParams);

				System.out.println(JSONObject.toJSONString(trades));*/

            //
//				HuobiAsset[] sses = accountService.getHuobiAssets();
//				TradeHistoryParams param = accountService.createFundingHistoryParams();
//				List<FundingRecord>  list = accountService.getFundingHistory(param);
			/*	HuobiAccount[] accounts = accountService.getAccounts();
				for(HuobiAccount acctemp : accounts){

				}*/
        } catch (Exception e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }

        //市场信息
		/*HuobiMarketDataService marketService = (HuobiMarketDataService)exchange.getMarketDataService();
			CurrencyPair pair = new CurrencyPair("BTC", "USDT");
			try {
//				Ticker ticker = marketService.getTicker(pair);
//				OrderBook orderBook = marketService.getOrderBook(pair);
				*//*HuobiDepth depth = marketService.getHuobiDepth(pair);
				System.out.println(JSONObject.toJSONString(depth));*//*
//				System.out.println(ticker.toString());
				*//*System.out.println(sdf.format(new Date()));
				HuobiTicker huobiTicker = marketService.getHuobiTicker(pair);
				System.out.println(huobiTicker.toString());
				System.out.println(sdf.format(new Date()));
//				Trades trades = marketService.getTrades(pair);
//				System.out.println(trades.toString());
				System.out.println(sdf.format(new Date()));*//*
				HuobiAssetPair[] arg =	marketService.getHuobiAssetPairs();
				System.out.println(JSONObject.toJSONString(arg));
				*//*for(HuobiAssetPair pai : arg){
					System.out.println(pai.toString());
				}*//*
			} catch (IOException e) {
				e.printStackTrace();
			}*/

			/*try {
				HuobiAssetPair[] arg =	marketService.getHuobiAssetPairs();
				for(HuobiAssetPair pai : arg){
					System.out.println(pai.toString());
				}
			} catch (IOException e) {
				e.printStackTrace();
			}*/

        //交易信息
	/*HuobiTradeService tradeService = (HuobiTradeService)exchange.getTradeService();
	    String orderId = "3834506596";
			try {
				//订单撤销
//				boolean flag = tradeService.cancelOrder(orderId);
				//所有挂单
				OpenOrders openOrders = tradeService.getOpenOrders();
				//交易历史查询
				TradeHistoryParams tradeHistoryParams = new TradeHistoryParams() {
					@Override
					public int hashCode() {
						return super.hashCode();
					}
				};
				UserTrades userTrades = tradeService.getTradeHistory(tradeHistoryParams);
				System.out.println("rrrrr");
			} catch (IOException e) {
				e.printStackTrace();
			}*/
			  /*  	tradeService.getTradeHistory(tradeHistoryParams);
				//限价格下单1、买或询问2、购买金额3、交易对4、订单号5、时间6、限价
				*//**
         * @param type Either BID (buying) or ASK (selling)
         * @param originalAmount The amount to trade
         * @param currencyPair The identifier (e.g. BTC/USD)
         * @param id An id (usually provided by the exchange)
         * @param timestamp a Date object representing the order's timestamp according to the exchange's
         *     server, null if not provided
         * @param limitPrice In a BID this is the highest acceptable price, in an ASK this is the lowest
         *     acceptable price
         *//*
				LimitOrder limitOrder=new LimitOrder(Order.OrderType.BID,new BigDecimal("0.0001"),new CurrencyPair("BTC","USDT"), UUID.randomUUID().toString(),new Date(),new BigDecimal("100000"));
				tradeService.verifyOrder(limitOrder);
				String resultLimit = tradeService.placeLimitOrder(limitOrder);
				//市场价格下单
				*//**
         * @param type Either BID (buying) or ASK (selling)
         * @param originalAmount The amount to trade
         * @param currencyPair The identifier (e.g. BTC/USD)
         * @param id An id (usually provided by the exchange)
         * @param timestamp a Date object representing the order's timestamp according to the exchange's
         *     server, null if not provided
         * @param averagePrice the weighted average price of any fills belonging to the order
         * @param status the status of the order at the exchange or broker
         *//*
				MarketOrder marketOrder = new MarketOrder(Order.OrderType.BID,new BigDecimal("0.0001"),new CurrencyPair("BTC","USDT"), UUID.randomUUID().toString(),new Date(),new BigDecimal("100000"),new BigDecimal("100000"),new BigDecimal("100000"), Order.OrderStatus.NEW);
				tradeService.verifyOrder(marketOrder);
				String resultMarket = tradeService.placeMarketOrder(marketOrder);
			} catch (IOException e) {
				e.printStackTrace();
			}*/

    }

    private static void bian() {
        Exchange exchange = ExchangeFactory.INSTANCE.createExchange(BinanceExchange.class.getName(), "8bcYzuqRdJnyaldjOQcGiU6Ew7M6IBSjbmYT42jpvF7qt8PPxlebwimfDyuQjdOq", "tM9vEtv4lOwjJqo3almZVOVSTdxPWjO1LhFL9buyZrwE0AaN8ejqHm3IFm5aXVxn");
        //账户信息
		/*BinanceAccountService accountService = (BinanceAccountService) exchange.getAccountService();
	    try {
	    	//账户查询
		    AccountInfo acc = accountService.getAccountInfo();
		    Map<Currency,Balance> balanceMap = acc.getWallet().getBalances();
			Map<Currency,Balance> retMap = new HashMap<Currency,Balance>();
			for(Currency currency : balanceMap.keySet()){
				if(balanceMap.get(currency).getTotal().compareTo(new BigDecimal(0))>0){
					retMap.put(currency,balanceMap.get(currency));
				}
			}
		    //取消订单历史
		    TradeHistoryParams param = accountService.createFundingHistoryParams();
		    List<FundingRecord>  list = accountService.getFundingHistory(param);

	    } catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}*/
        //市场信息
        CurrencyPair pair = new CurrencyPair("BTC", "USDT");
        BinanceMarketDataService marketService = (BinanceMarketDataService) exchange.getMarketDataService();
//	    marketService.aggTrades(pair, fromId, startTime, endTime, limit);
//	    marketService.getBinanceOrderbook(pair, limit);
			/*try {
//				Ticker ticker = marketService.getTicker(pair);
				OrderBook orderBook = marketService.getOrderBook(pair);
//				BinanceOrderbook binanceOrderbook = marketService.getBinanceOrderbook(pair,5);
				//bi
//				System.out.println(JSONObject.toJSONString(orderBook));
				List<BinanceKline> binanceKlines = marketService.klines(pair, KlineInterval.d1);
			} catch (IOException e) {
				e.printStackTrace();
			}*/

        //交易信息
        BinanceTradeService tradeService = (BinanceTradeService) exchange.getTradeService();
        try {
            CurrencyPair currencyPair = new CurrencyPair("ETH", "USDT");
            BinanceTradeHistoryParams binanceTradeHistoryParams = new BinanceTradeHistoryParams(currencyPair);
            UserTrades trades = tradeService.getTradeHistory(binanceTradeHistoryParams);
//                System.out.println(JSONObject.toJSONString(trades.getTrades()));
            BigDecimal finalPrice = new BigDecimal(0);

            for (Trade trade : trades.getTrades()) {
                UserTrade userTrade = (UserTrade) trade;
                if (userTrade.getOrderId().equals("69997450")) {
                    System.out.println("sdf");
                    finalPrice = trade.getPrice().multiply(trade.getOriginalAmount());
                    break;
                }
                System.out.println(JSONObject.toJSONString(trade));
            }
            System.out.println(finalPrice);
            //订单撤销
				/*String orderId = "";
				boolean flag = tradeService.cancelOrder(orderId);
				tradeService.getOpenOrders();
				LimitOrder limitOrder=new LimitOrder(Order.OrderType.BID,new BigDecimal("0.0001"),new CurrencyPair("BTC","USDT"), UUID.randomUUID().toString(),new Date(),new BigDecimal("100000"));
				tradeService.verifyOrder(limitOrder);
				String resultLimit = tradeService.placeLimitOrder(limitOrder);
				MarketOrder marketOrder = new MarketOrder(Order.OrderType.BID,new BigDecimal("0.0001"),new CurrencyPair("BTC","USDT"), UUID.randomUUID().toString(),new Date(),new BigDecimal("100000"),new BigDecimal("100000"),new BigDecimal("100000"), Order.OrderStatus.NEW);
				tradeService.verifyOrder(marketOrder);
				String resultMarket = tradeService.placeMarketOrder(marketOrder);
				tradeService.createOpenOrdersParams();
				tradeService.createTradeHistoryParams()*/
            ;
        } catch (IOException e) {
            e.printStackTrace();
        }

    }

}

