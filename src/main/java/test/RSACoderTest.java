package test;

import com.mico.micochange.common.RSACoder;

import java.util.Map;

public class RSACoderTest {
    private static String publicKey = "MIGfMA0GCSqGSIb3DQEBAQUAA4GNADCBiQKBgQCy+DLh4kTPEOmI40Ui0LG3x/Rf6qjlx2ZroyND5HDdn9aPbZC6jkIhNQ1eQeAvqfELigSVNM+ghYhXeqqCviNi1ddaXW+OrpBYAkNTDqeIIylDmT3IceRqhC0QSk8GrfrhOiLb9IwBJeSZiYRMeFuV7nd1Iqdp654A7s19nrnv+wIDAQAB";
    private static String privateKey = "MIICdgIBADANBgkqhkiG9w0BAQEFAASCAmAwggJcAgEAAoGBALL4MuHiRM8Q6Yjj\n" +
            "RSLQsbfH9F/qqOXHZmujI0PkcN2f1o9tkLqOQiE1DV5B4C+p8QuKBJU0z6CFiFd6\n" +
            "qoK+I2LV11pdb46ukFgCQ1MOp4gjKUOZPchx5GqELRBKTwat+uE6Itv0jAEl5JmJ\n" +
            "hEx4W5Xud3Uip2nrngDuzX2eue/7AgMBAAECgYEAqwilX9ekWJraA/bmMGJvJKpT\n" +
            "sYKJjKZmDAGpBw3+it5g50sXICIpsqCHuQeNnx9ye8uXm5wPvGFArhFNeIsLNqNF\n" +
            "j5Dk1J5XWITTrWm3fM0dAxd0+ua//12UfzxeOWb5BoLEO6joyqAnjVCGuf3ALzLT\n" +
            "kDAFZ5/E3TJS3Aq0BFECQQDqZdVlpEEXzKpu2pI+Xp7IYiyo1+sBILQ+lJD/BRIF\n" +
            "D+6Avq0thc0ze2yQBdnbGF/XPkGy6qc8SHA5iO2Slj5zAkEAw3akQWDq2R2lp1a1\n" +
            "1D3qCvzYwD1O2wetZ2MWZye2TiRui67Aie5+XfVp4IP1pKfvG7qQY/aO6J8n0dfE\n" +
            "GDBeWQJAehuUwL+BvYE8ZEVvHBi0xRUHyOm6njrISzWgF4ovqf1ztRQgKW/jx2cL\n" +
            "1jeGF3IqM3pWRXuipW/jLsXbZZrpawJADApR4ekbblZiLuWre7B4q0aZ/3WHVvyp\n" +
            "FNZIuf/7PeMGoDoaiCSVyink5LycNBFYLEgYvt+gO/oZ1NZKsQbBuQJACR/jdG0A\n" +
            "5cmX8m3sKEcDhYGZT3kOLC3E6Dqt0e69+E6EED/azp874XeREgPymPS3yM5f8OWw\n" +
            "Xe4F45iePK6PXw==";

    /*
     * 非对称加密算法   RSA过程 ： 以甲乙双方为例
     *      1、初始化密钥 构建密钥对,生成公钥、私钥保存到keymap中
     *              KeyPairGenerator --->    KeyPair     -->      RSAPublicKey、RSAPrivateKey
     *      2、甲方使用私钥加密, 加密后在用私钥对加密数据进行数据签名，然后发送给乙方
     *              RSACoder.encryptByPrivateKey(data, privateKey);
     *              RSACoder.sign(encodedData, privateKey);
     *      3、乙方则通过公钥验证签名的加密数据，如果验证正确则在通过公钥对加密数据进行解密
     *              RSACoder.verify(encodedData, publicKey, sign);
     *              RSACoder.decryptByPublicKey(encodedData, publicKey);
     *
     *      4、乙方在通过公钥加密发送给甲方
     *              RSACoder.encryptByPublicKey(decodedData, publicKey);
     *      5、甲方通过私钥解密该数据
     *              RSACoder.decryptPrivateKey(encodedData, privateKey);
     */
    public static void setUp() throws Exception {

        Map<String, Object> keyMap = RSACoder.initKey();

        publicKey = RSACoder.getPublicKey(keyMap);
        privateKey = RSACoder.getPrivateKey(keyMap);

        System.out.println("公钥：\n" + publicKey);
        System.out.println("私钥：\n" + privateKey);
    }


    public void test() throws Exception {
        String inputStr = "abc";
        byte[] data = inputStr.getBytes();//每次的得到的字节数组是不一样的。
        //第二步 私钥加密
        byte[] encodedData = RSACoder.encryptByPrivateKey(data, privateKey);
        //私钥进行数据签名
        String sign = RSACoder.sign(encodedData, privateKey);

        //第三步 公钥验证数字签名
        boolean flag = RSACoder.verify(encodedData, publicKey, sign);
        System.out.println("flag:" + flag);
        //用公钥对数据解密
        byte[] decodedData = RSACoder.decryptByPublicKey(encodedData, publicKey);

        System.out.println("data:" + data + "加密数据：" + encodedData + "    解密数据：" + decodedData);
        System.out.println("加密前数据-：" + new String(data) + "     解密后数据： " + new String(decodedData));

        //第四步使用公钥加密数据
        encodedData = RSACoder.encryptByPublicKey(decodedData, publicKey);

        //第五步 使用私钥解密数据
//        decodedData = RSACoder.decryptPrivateKey(encodedData, privateKey);


        System.out.println("data:" + data + "加密数据：" + encodedData + "    解密数据：" + decodedData);
        System.out.println("加密前数据：" + inputStr + "     解密后数据： " + new String(decodedData));
    }


    public static void test1() throws Exception {
        System.out.println("私钥加密-----公钥解密");
        String inputStr = "some data to sign";
        byte[] data = inputStr.getBytes("UTF-8");
        System.out.println("data:" + data);

        byte[] encodedData = RSACoder.encryptByPrivateKey(data, privateKey);
        byte[] decodedData = RSACoder.decryptByPublicKey(encodedData, publicKey);

        String outputStr = new String(decodedData);


        System.out.println("加密前：" + inputStr + "\n 解密后：" + outputStr);


        String encodedData1 = "33b147151933c0c0b6f3a223767105340d5cf533c68b42d3fae5884aa76d3cee90e1130d0606f8224b4b9c18d101fe76e90e2a48d0ba25a609357e968c03be029b43b17b111b2e715978de56cfa3456abd000c2ea86828cdc0f3d86515ac050a5f46882fc8171873f44b3f086ca2e24f5f9fb884bdb85f2c840ce41a29374e44";
        /*byte[] decodedData1 = RSACoder.decryptByPublicKey(encodedData1.getBytes(), publicKey);
        String outputStr1 = new String( decodedData1 );
        System.out.println("%%%%%"+outputStr1);*/

        System.out.println("私钥签名---公钥验证签名");
        //产生签名
        String sign = RSACoder.sign(data, privateKey);
        System.out.println("签名：\r" + sign);
        System.out.println(encodedData1);

        //验证签名
        boolean flag = RSACoder.verify(data, publicKey, sign);

        System.out.println("状态：\r" + flag);
    }

    public static void main(String[] strs) {
        try {
            test1();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
