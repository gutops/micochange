package test;

import com.mico.micochange.client.MinaClient;

/**
 * Created by chenst on 2018/5/5.
 */
public class MinaClientThread extends Thread {

    private String msg;

    public void setMsg(String msg) {
        this.msg = msg;
    }

    @Override
    public void run() {
        MinaClient client = new MinaClient();
        client.sendMessage(msg);
    }
}
